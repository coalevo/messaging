/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.model;

import java.util.Iterator;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Map;

/**
 * Defines the interface for a <tt>InteractiveMessage</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface InteractiveMessage
    extends Message {

  /**
   * Sets the type of this <tt>InteractiveMessage</tt>.
   *
   * @param t the type as {@link InteractiveMessageType}.
   */
  public void setType(InteractiveMessageType t);

  /**
   * Returns the type of this <tt>InteractiveMessage</tt>.
   *
   * @return the type as {@link InteractiveMessageType}.
   */
  public InteractiveMessageType getType();

  /**
   * Returns the thread of this <tt>InteractiveMessage</tt>.
   *
   * @return the thread of this <tt>InteractiveMessage</tt> as <tt>String</tt>.
   */
  public String getThread();

  /**
   * Tests if this <tt>InteractiveMessage</tt> has a thread.
   *
   * @return true if this <tt>InteractiveMessage</tt> has a thread set, false otherwise.
   */
  public boolean hasThread();

  /**
   * Tests if this <tt>InteractiveMessage</tt> thread value equals the
   * given value.
   *
   * @param thread a thread identifier value as <tt>String</tt>.
   * @return true if thread identifiers are equal, false otherwise.
   */
  public boolean isThread(String thread);

  /**
   * Returns the subject of this <tt>InteractiveMessage</tt>
   * in the given <tt>Locale</tt>.
   *
   * @param l a <tt>Locale</tt> instance.
   * @return the subject as <tt>String</tt>.
   * @throws NoSuchElementException if a subject for the given <tt>Locale</tt>
   *                                does not exist.
   * @see #hasSubject(Locale)
   */
  public String getSubject(Locale l);

  /**
   * Returns the subject of this <tt>InteractiveMessage</tt>
   * in the default or first available <tt>Locale</tt>.
   *
   * @return the subject as <tt>String</tt> or null if it does not exist.
   */
  public String getSubject();

  /**
   * Returns an iterator over a set of <tt>Map.Entry</tt> instances,
   * holding the language (=key) the subject (=value)
   * of this <tt>InteractiveMessage</tt>.
   *
   * @return an <tt>Iterator</tt> over <tt>Map.Entry</tt> instances.
   */
  public Iterator<Map.Entry<Locale,String>> getSubjects();

  /**
   * Tests if this <tt>InteractiveMessage</tt> has a subject
   * in the given <tt>InteractiveMessage</tt>.
   *
   * @param l a <tt>Locale</tt> instance.
   * @return true if a subject in the given <tt>Locale</tt> exists,
   *         false otherwise.
   */
  public boolean hasSubject(Locale l);

  /**
   * Tests if this <tt>InteractiveMessage</tt> has a subject
   * in the default or first available <tt>Locale</tt>.
   *
   * @return true if a subject in the default or first available <tt>Locale</tt> exists,
   *         false otherwise.
   */
  public boolean hasSubject();

  /**
   * Tests if this <tt>InteractiveMessage</tt> has subjects in
   * any <tt>Locale</tt>.
   *
   * @return true if subjects are available, false otherwise.
   */
  public boolean hasSubjects();

  /**
   * Returns the body of this <tt>InteractiveMessage</tt>
   * in the given <tt>Locale</tt>.
   *
   * @param l a <tt>Locale</tt> instance.
   * @return the body as <tt>String</tt>.
   * @throws NoSuchElementException if a subject for the given <tt>Locale</tt>
   *                                does not exist.
   * @see #hasBody(Locale)
   */
  public String getBody(Locale l);

  /**
   * Returns the body of this <tt>InteractiveMessage</tt>
   * in the default or first available <tt>Locale</tt>.
   *
   * @return the body as <tt>String</tt> or null if it does not exist.
   */
  public String getBody();

  /**
   * Returns an iterator over a set of <tt>Map.Entry</tt> instances,
   * holding the language (=key) the body (=value)
   * of this <tt>InteractiveMessage</tt>.
   *
   * @return an <tt>Iterator</tt> over <tt>Map.Entry</tt> instances.
   */
  public Iterator<Map.Entry<Locale,String>> getBodies();

  /**
   * Tests if this <tt>InteractiveMessage</tt> has a body
   * in the given <tt>Locale</tt>.
   *
   * @param l a <tt>Locale</tt> instance.
   * @return true if a body in the given <tt>Locale</tt> exists,
   *         false otherwise.
   */
  public boolean hasBody(Locale l);

  /**
   * Tests if this <tt>InteractiveMessage</tt> has a body
   * in the default or first available <tt>Locale</tt>.
   *
   * @return true if a body in the default or first available <tt>Locale</tt> exists,
   *         false otherwise.
   */
  public boolean hasBody();

  /**
   * Tests if this <tt>InteractiveMessage</tt> has bodies in
   * any <tt>Locale</tt>.
   *
   * @return true if bodies are available, false otherwise.
   */
  public boolean hasBodies();

  /**
   * Tests if this <tt>InteractiveMessage</tt>
   * requires confirmation.
   *
   * @return true if confirmation is required, false otherwise.
   */
  public boolean isConfirmationRequired();

  /**
   * Tests if this <tt>InteractiveMessage</tt> is a broadcast.
   *
   * @return true if the message is a broadcast, false otherwise.
   */
  public boolean isBroadcast();

  /**
   * Returns the default <tt>Locale</tt> of this <tt>Message</tt>.
   *
   * @return an <tt>Locale</tt> instance.
   */
  public Locale getLocale();

  /**
   * Tests if this <tt>Message</tt> has a
   * <tt>Locale</tt>.
   *
   * @return true if this <tt>Message</tt> has a
   *         <tt>Locale</tt> false otherwise.
   */
  public boolean hasLocale();

  /**
   * Returns an unmodifiable {@link EventInfo} instance
   * with the information from this Message.
   *
   * @return an unmodifiable {@link EventInfo}.
   */
  public EventInfo getEventInfo();

  /**
   * Return a string that indicates a way transform a message.
   * <p>
   * Note:<br/>
   * This hint does not imply that the client may be able to format
   * the message using the indicated transform.
   * </p>
   * 
   * @return a string that identifies a message formatting hint.
   */
  public String getFormattingHint();

  /**
   * Tests if this Message has a formatting hint.
   * @return true if it has a hint, false otherwise.
   */
  public boolean hasFormattingHint();

}//interface InteractiveMessage
