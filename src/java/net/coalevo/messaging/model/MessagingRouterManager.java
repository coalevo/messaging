/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.model;

import net.coalevo.messaging.service.MessagingService;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Defines a <tt>MessagingRouterManager</tt> that handles
 * {@link MessagingRouter} instances.
 * <p/>
 * The suggested implementation should operate in whiteboard
 * mode. If a {@link MessagingRouter} is registered as service
 * in the OSGi container, the implementation should automatically
 * pick it up and make it available for the {@link MessagingService}
 * implementation.
 * </p>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface MessagingRouterManager {

  /**
   * Returns the {@link MessagingRouter} for the given domain.
   *
   * @param domain the domain as <tt>String</tt>.
   * @return a {@link MessagingRouter} instance.
   * @throws NoSuchElementException if no router is available for the given domain.
   */
  public MessagingRouter get(String domain);

  /**
   * Tests if a {@link MessagingRouter} is available for the given domain.
   *
   * @param domain the domain as <tt>String</tt>.
   * @return true if available, false otherwise.
   */
  public boolean isAvailable(String domain);

  /**
   * Returns an iterator over all available routers.
   *
   * @return an <tt>Iterator</tt> over String instances representing domain names for which
   *         routers are available.
   */
  public Iterator<String> listAvailable();

}//interface MessagingRouterManager
