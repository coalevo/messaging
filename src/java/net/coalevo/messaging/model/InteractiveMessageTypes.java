/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.model;

import java.util.*;
import java.io.Serializable;

/**
 * Provides all defined {@link InteractiveMessageType}s.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class InteractiveMessageTypes {

  /**
   * Defines an {@link InteractiveMessageType} for a chat message.
   * The context of a {@link InteractiveMessage} of this type is
   * a thread or the sender/address.
   */
  public static final InteractiveMessageType CHAT = new TypeImpl("chat");

  /**
   * Defines an {@link InteractiveMessageType} for a message in a
   * group chat. The context of this message is the group.
   */
  public static final InteractiveMessageType GROUPCHAT = new TypeImpl("groupchat");

  /**
   * Defines an {@link InteractiveMessageType} for a message that is
   * giving some news. This message does not require a reply and
   * will likely be produced automatically by some service.
   */
  public static final InteractiveMessageType NEWS = new TypeImpl("news");

 /**
  * Defines an {@link InteractiveMessageType} for an offline message.
  * The context of a {@link InteractiveMessage} of this type is
  * a thread or the sender/address, but the message may have been stored on
  * the server for a while.
  */
  public static final InteractiveMessageType OFFLINE = new TypeImpl("offline");

  /**
   * Defines an {@link InteractiveMessageType} for a message
   * that is notifying the user of something. This message will not
   * require a reply, and will likely be produced by some service of
   * the system.
   */
  public static final InteractiveMessageType NOTIFICATION = new TypeImpl("notification");

 /**
   * Defines an {@link InteractiveMessageType} for a message
   * that is a question from a user to a system guide.
   */
  public static final InteractiveMessageType QUESTION = new TypeImpl("question");

  /**
   * Array holding the defined {@link InteractiveMessageType} instances.
   */
  protected static final InteractiveMessageType[] TYPES =
      new InteractiveMessageType[]{CHAT, GROUPCHAT, NEWS, OFFLINE, NOTIFICATION, QUESTION};

  /**
   * Set holding the defined {@link InteractiveMessageType} instances.
   */
  protected static final Set<InteractiveMessageType> INTERACTIVEMESSAGE_TYPES =
      Collections.unmodifiableSet(new HashSet<InteractiveMessageType>(Arrays.asList(TYPES)));

  /**
   * Returns a {@link InteractiveMessageType} instance for the given identifier.
   *
   * @param identifier a string identifying a {@link InteractiveMessageType}.
   * @return the {@link InteractiveMessageType} with the given identifier.
   * @throws NoSuchElementException if a {@link InteractiveMessageType} with
   *                                the given identifier does not exist.
   */
  public final static InteractiveMessageType get(String identifier)
      throws NoSuchElementException {
    for (InteractiveMessageType imType : INTERACTIVEMESSAGE_TYPES) {
      if (imType.getIdentifier().equals(identifier)) {
        return imType;
      }
    }
    throw new NoSuchElementException(identifier);
  }//get


  static final class TypeImpl
      implements InteractiveMessageType, Serializable {

    private final String m_Identifier;

    protected TypeImpl(String id) {
      m_Identifier = id;
    }//constructor

    public String getIdentifier() {
      return m_Identifier;
    }//getIdentifier

    public String toString() {
      return m_Identifier;
    }//toString

    public boolean equals(Object o) {
      if (this == o) return true;
      if (!(o instanceof TypeImpl)) return false;

      final TypeImpl type = (TypeImpl) o;

      if (!m_Identifier.equals(type.m_Identifier)) return false;

      return true;
    }//equals

    public int hashCode() {
      return (m_Identifier != null ? m_Identifier.hashCode() : 0);
    }//hashCode

  }//inner class TypeImpl

}//class InteractiveMessageTypes
