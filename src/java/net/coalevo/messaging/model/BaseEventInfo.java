/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.model;

import net.coalevo.foundation.model.AgentIdentifier;

/**
 * Abstract base class for {@link EventInfo} implementations.
 * <p/>
 * Standardizes the {@link #equals(Object)} method to compare
 * identifier, to and from.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class BaseEventInfo
    implements EventInfo {

  private final String m_Identifier;
  private final AgentIdentifier m_From;
  private final AgentIdentifier m_To;
  private final boolean m_Multicast;
  private final long  m_Created = System.currentTimeMillis();

  /**
   * Constructs the instance with final information.
   *
   * @param identifier the identifier of a message/event.
   * @param from the sender as {@link AgentIdentifier}.
   * @param to the addressee as {@link AgentIdentifier}.
   */
  protected BaseEventInfo(String identifier, AgentIdentifier from, AgentIdentifier to) {
    m_Identifier = identifier;
    m_To = to;
    m_From = from;
    m_Multicast = false;
  }//constructor

  /**
   * Constructs the instance with final information.
   *
   * @param identifier the identifier of a message/event.
   * @param from the sender as {@link AgentIdentifier}.
   * @param to the addressee as {@link AgentIdentifier}.
   * @param b a flag that indicates if this is a multicast generated event info.
   */
  protected BaseEventInfo(String identifier, AgentIdentifier from, AgentIdentifier to,boolean b) {
       m_Identifier = identifier;
    m_To = to;
    m_From = from;
    m_Multicast = b;
  }//constructor

  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  public AgentIdentifier getTo() {
    return m_To;
  }//getTo

  public AgentIdentifier getFrom() {
    return m_From;
  }//getFrom

  public boolean isMulticast() {
    return m_Multicast;
  }//isMulticast

  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || !(o instanceof BaseEventInfo)) return false;

    final BaseEventInfo eventInfo = (BaseEventInfo) o;

    if (m_From != null ? !m_From.equals(eventInfo.m_From) : eventInfo.m_From != null) return false;
    if (m_Identifier != null ? !m_Identifier.equals(eventInfo.m_Identifier) : eventInfo.m_Identifier != null)
      return false;
    if (m_To != null ? !m_To.equals(eventInfo.m_To) : eventInfo.m_To != null) return false;

    return true;
  }//equals

  public long getCreated() {
    return m_Created;
  }//getCreated

  public int hashCode() {
    int result;
    result = (m_Identifier != null ? m_Identifier.hashCode() : 0);
    result = 29 * result + (m_From != null ? m_From.hashCode() : 0);
    result = 29 * result + (m_To != null ? m_To.hashCode() : 0);
    return result;
  }//hashCode


}//class BaseEventInfo
