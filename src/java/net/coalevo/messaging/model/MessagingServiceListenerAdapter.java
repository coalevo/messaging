/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.model;

import net.coalevo.foundation.model.AgentIdentifier;

/**
 * Abstract base class that provides a dummy
 * {@link MessagingServiceListener}, and may be extended to implement
 * the methods of interest only.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class MessagingServiceListenerAdapter
    implements MessagingServiceListener {

    public boolean received(InteractiveMessage m) {
      return true;
    }//received

    public boolean received(Request r) {
      return true;
    }//received

    public boolean received(Response r) {
      return true;
    }//received

    public void confirmed(EventInfo ei, long timestamp) {
    }//confirmed

    public void failedDelivery(Message m) {
    }//failedDelivery

    public void startedMessage(AgentIdentifier from, String msgid) {
    }//startedMessage

    public void stoppedMessage(AgentIdentifier from, String msgid) {
    }//stoppedMessage

}//class MessagingServiceListenerAdapter
