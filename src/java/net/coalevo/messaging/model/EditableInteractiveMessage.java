/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.model;

import net.coalevo.foundation.model.AgentIdentifier;

import java.util.Locale;

/**
 * Defines the interface for an editable {@link InteractiveMessage}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface EditableInteractiveMessage
    extends InteractiveMessage {

  /**
   * Sets the addressee of this {@link InteractiveMessage}.
   *
   * @param to the addressee as {@link AgentIdentifier}.
   */
  public void setTo(AgentIdentifier to);

  /**
   * Sets the thread of this {@link InteractiveMessage}.
   *
   * @param str the thread of this {@link InteractiveMessage} as <tt>String</tt>.
   */
  public void setThread(String str);

  /**
   * Sets the subject of this {@link InteractiveMessage}
   * assuming it to be in the actual {@link Locale}.
   *
   * @param subject the subject as <tt>String</tt>.
   * @see #getLocale()
   */
  public void setSubject(String subject);

  /**
   * Sets the subject of this {@link InteractiveMessage}
   * in the given {@link Locale}.
   *
   * @param l a {@link Locale} instance.
   * @param s the subject as <tt>String</tt>.
   */
  public void setSubject(Locale l, String s);

  /**
   * Removes the subject of this {@link InteractiveMessage}
   * in the given {@link Locale}.
   *
   * @param l a {@link Locale} instance.
   */
  public void removeSubject(Locale l);

  /*
  * Sets the body of this {@link InteractiveMessage}
  * assuming it to be in the actual {@link Locale}.
  *
  * @param body the body as <tt>String</tt>.
  * @see #getLocale()
  */
  public void setBody(String body);

  /**
   * Sets the body of this {@link InteractiveMessage}
   * in the given {@link Locale}.
   *
   * @param l a {@link Locale} instance.
   * @param b the body as <tt>String</tt>.
   */
  public void setBody(Locale l, String b);

  /**
   * Removes the body of this <tt>Instant<essage</tt>
   * in the given {@link Locale}.
   *
   * @param l a {@link Locale} instance.
   */
  public void removeBody(Locale l);

  /**
   * Sets the flag that indicates that this {@link InteractiveMessage}
   * requires confirmation.
   *
   * @param b true if confirmation required, false otherwise.
   */
  public void setConfirmationRequired(boolean b);

  /**
   * Sets the formatting hint for this message.
   * @param hint the formatting hint, identifying a possible transform.
   */
  public void setFormattingHint(String hint);

}//interface EditableInteractiveMessage
