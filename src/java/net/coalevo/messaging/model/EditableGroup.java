/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.model;

import net.coalevo.foundation.model.AgentIdentifier;

import java.util.Locale;
import java.util.Set;

/**
 * This interface defines an editable group.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface EditableGroup
    extends Group {

  /**
   * Sets the language of this group.
   *
   * @param l language of this group as <tt>Locale</tt>.
   */
  public void setLanguage(Locale l);

  /**
   * Sets a string defining the format of this group's info.
   *
   * @param format a string defining the info format.
   */
  public void setInfoFormat(String format);

  /**
   * Sets the moderator of this group.
   *
   * @param aid an {@link AgentIdentifier} instance.
   */
  public void setModerator(AgentIdentifier aid);

  /**
   * Sets all tags of this group.
   *
   * @param tags a set of strings.
   */
  public void setTags(Set<String> tags);

  /**
   * Adds a tag to the group.
   *
   * @param tag a tag String.
   */
  public void addTag(String tag);

  /**
   * Removes a tag from this group.
   *
   * @param tag a tag String.
   */
  public void removeTag(String tag);

  /**
   * Sets the flag that determines if this group is invite only.
   * <p/>
   * Invite only means that the moderator has to invite users
   * to the group.
   *
   * @param b true if invite only, false otherwise.
   */
  public void setInviteOnly(boolean b);

  /**
   * Sets the flag that determines if invites to this group
   * are moderated.
   * <p/>
   * Invite only means that the moderator has to invite users
   * to the group.
   *
   * @param b true if invite only, false otherwise.
   */
  public void setInviteModerated(boolean b);

  /**
   * Returns the corresponding {@link Group} instance.
   *
   * @return the corresponding {@link Group} instance.
   */
  public Group toGroup();

}//interface EditableGroup
