/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.model;

import net.coalevo.presence.model.ServicePresenceProxy;
import net.coalevo.presence.model.ServicePresenceProxyListener;
import net.coalevo.messaging.service.MessagingService;
import org.osgi.framework.*;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.concurrent.CountDownLatch;

/**
 * This class implements a proxy for a service that wants to use the {@link MessagingService}.
 * <p/>
 * This proxy provides a reference to an available {@link MessagingService} and ensures that
 * the actual presence instance is registered with the service.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ServiceMessagingProxy
    implements ServicePresenceProxyListener {

  private Logger m_Log;
  private Marker m_LogMarker;

  private BundleContext m_BundleContext;
  private ServicePresenceProxy m_ServicePresence;

  private MessagingService m_MessagingService;
  private CountDownLatch m_MessagingServiceLatch;

  private MessagingServiceListener m_MessagingServiceListener;
  
  public ServiceMessagingProxy(ServicePresenceProxy spp, Logger log) {
    m_Log = log;
    m_LogMarker = MarkerFactory.getMarker("ServiceMessagingProxy");
    m_ServicePresence = spp;
    m_MessagingServiceListener = new MessagingServiceListenerAdapter(){};
  }//ServiceMessagingProxy

  public ServiceMessagingProxy(MessagingServiceListener msl, ServicePresenceProxy spp, Logger log) {
    m_Log = log;
    m_LogMarker = MarkerFactory.getMarker("ServiceMessagingProxy");
    m_ServicePresence = spp;
    m_MessagingServiceListener = msl;
  }//ServiceMessagingProxy

  public boolean activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    m_MessagingServiceLatch = new CountDownLatch(1);
    //prepareDefinitions listener
    ServiceListener serviceListener = new ServiceListenerImpl();

    //prepareDefinitions the filter
    String filter =
        "(objectclass=" + MessagingService.class.getName() + ")";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(serviceListener, filter);

      //ensure that already registered Service instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        serviceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      ex.printStackTrace(System.err);
      return false;
    }
    //Add listener after first available presence
    m_ServicePresence.getPresence();
    m_ServicePresence.addListener(this);
    return true;
  }//activate

  public void deactivate() {
    if(m_ServicePresence != null) {
      m_ServicePresence.removeListener(this);
    }

    if(m_MessagingService != null) {
      m_MessagingService.unregister(m_ServicePresence.getPresence());
      m_MessagingService = null;
    }
    m_MessagingServiceLatch = null;
    m_BundleContext = null;
  }//deactivate

   public MessagingService getMessagingService() {
    try {
      m_MessagingServiceLatch.await();
    } catch (InterruptedException e) {
      m_Log.error(m_LogMarker, "getMessagingService()", e);
    }
    return m_MessagingService;
  }//getPresence

  private void initMessaging() {
    MessagingService ms = getMessagingService();
    boolean b = ms.register(m_ServicePresence.getPresence(),m_MessagingServiceListener);
    m_Log.debug(m_LogMarker, "initMessaging()::" + b);
  }//initMessaging

  public void updatedPresence() {
    //TODO: maybe we need a more complex mechanism to unregister and register?    
    initMessaging();
  }//updatedPresence

  private class ServiceListenerImpl
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof MessagingService) {
            m_MessagingService = (MessagingService) o;
            m_MessagingServiceLatch.countDown();
            initMessaging();
            m_Log.debug(m_LogMarker, "serviceChanged()::registered::MessagingService");
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof MessagingService) {
            m_MessagingService = null;
            m_MessagingServiceLatch = new CountDownLatch(1);
            m_Log.debug(m_LogMarker, "serviceChanged()::unregistered::MessagingService");
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
      }
    }
  }//inner class ServiceListenerImpl

}//class ServiceMessagingProxy
