/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.model;

/**
 * Defines a router for the messaging service that can route
 * out-of-node communication over the network to another node.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface MessagingRouter {

  /**
   * Returns the domain this <tt>MessagingRouter</tt> routes to.
   *
   * @return the domain this <tt>MessagingRouter</tt> will handle as <tt>String</tt>.
   */
  public String getDomain();

  /**
   * Tests if this router is responsible for a given {@link Message}.
   * @param m the {@link Message} to be routed.
   * @return true if message should be routed over this router, false otherwise.
   */
  public boolean routes(Message m);

  /**
   * Routes a {@link Message}.
   *
   * @param m the {@link Message} to be routed.
   */
  public void route(Message m);

  /**
   * Routes a broadcast {@link InteractiveMessage}.
   *
   * @param m the {@link InteractiveMessage} to be routed and broadcasted.
   */
  public void routeBroadcast(InteractiveMessage m);

}//interface MessagingRouter
