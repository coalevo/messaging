/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.model;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Identifiable;


/**
 * Defines the interface for a {@link Message}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Message
    extends Identifiable, EventInfo {

  /**
   * Returns the sender.
   *
   * @return an {@link AgentIdentifier} instance representing the sender.
   */
  public AgentIdentifier getFrom();

  /**
   * Returns the addressee.
   *
   * @return an {@link AgentIdentifier} instance representing the addressee.
   */
  public AgentIdentifier getTo();

}//interface Message
