/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.model;

import net.coalevo.foundation.model.AgentIdentifier;

import java.util.Set;
import java.util.Locale;

/**
 * Defines the interface for a messaging group.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Group {

  /**
   * Returns the name of this group.
   *
   * @return the name of this group.
   */
  public String getName();

  /**
   * Returns the language of this group.
   * @return the language of this group as <tt>Locale</tt>.
   */
  public Locale getLanguage();

  /**
   * Returns the info about this group.
   *
   * @return the info as String.
   */
  public String getInfo();

  /**
   * Sets the info about this group.
   *
   * @param info the info about this group as String.
   */
  public void setInfo(String info);

  /**
   * Returns a string defining the format of the forum info.
   *
   * @return a string defining the info format.
   */
  public String getInfoFormat();

  /**
   * Returns the identifier of the moderator of the group.
   *
   * @return an {@link AgentIdentifier} instance.
   */
  public AgentIdentifier getModerator();

  /**
   * Returns a list of tags attached to thr entry.
   *
   * @return a <tt>List</tt> of String tags.
   */
  public Set<String> getTags();

  /**
   * Returns an unmodifiable set of present group members.
   * <p/>
   * @return a <tt>Set</tt> of present group members.
   */
  public Set<AgentIdentifier> getPresentMembers();
  
  /**
   * Returns an unmodifiable list of the actual group members.
   * <p/>
   * Note that for non permanent groups, the returned set will
   * always be equivalent to {@link #getPresentMembers()}.
   *
   * @return a <tt>Set</tt> of group members.
   */
  public Set<AgentIdentifier> getMembers();

  /**
   * Returns an unmodifiable set of invited user agents.
   * @return a <tt>Set</tt> of invited user agents.
   */
  public Set<AgentIdentifier> getInvited();

  /**
   * The creation date of the forum.
   *
   * @return the creation date as UTC timestamp.
   */
  public long getCreated();

  /**
   * Tests if the group is invite only.
   * <p/>
   * If the group is invite only and invite moderated, then the
   * moderator has to invite users to be able to join this group.
   * If the group is invite only but not moderated, then
   * any existing member may invite other users to this group.
   *
   * @return true if invite only, false otherwise.
   */
  public boolean isInviteOnly();

  /**
   * Tests if this group is moderated.
   * <p/>
   * This flag is used in conjunction with the invite only flag
   * to decide whether all users or only the moderator may invite users.
   *
   * @return true if the invites are moderated, false otherwise.
   */
  public boolean isInviteModerated();

  /**
   * Tests if this group is permanent.
   * <p/>
   * A permament group will remain even without present
   * members, and it's state will be persisted to a backend store.
   *
   * @return true if permanent, false otherwise.
   */
  public boolean isPermanent();

}//interface Group
