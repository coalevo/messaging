/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.model;

import net.coalevo.foundation.model.AgentIdentifier;

/**
 * Defines the interface for a <tt>MessagingServiceListener</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface MessagingServiceListener {

  /**
   * Received an {@link InteractiveMessage}.
   *
   * @param m an {@link InteractiveMessage}.
   * @return true if successfull, false otherwise.
   */
  public boolean received(InteractiveMessage m);

  /**
   * Received a {@link Request}.
   *
   * @param r a {@link Request}.
   * @return true if successfull, false otherwise.
   */
  public boolean received(Request r);

  /**
   * Received a {@link Response}.
   *
   * @param r a {@link Response}.
   * @return true if successfull, false otherwise.
   */
  public boolean received(Response r);

  /**
   * Received a delivery confirmation for a {@link InteractiveMessage}
   * that required delivery confirmation.
   *
   * @param ei        {@link EventInfo} holding the information about the
   *                   message that is being confirmed.
   * @param timestamp the timestamp of the confirmation as <tt>long</tt>.
   */
  public void confirmed(EventInfo ei, long timestamp);

  /**
   * Notice of a delivery failure.
   *
   * @param m an {@link InteractiveMessage}.
   */
  public void failedDelivery(Message m);

  /**
   * Notice that a specific {@link AgentIdentifier} started a
   * message.
   *
   * @param from  the {@link AgentIdentifier} that started a message.
   * @param msgid the identifier of the message.
   */
  public void startedMessage(AgentIdentifier from, String msgid);

  /**
   * Notice that a specific {@link AgentIdentifier} has aborted
   * a message.
   *
   * @param from  the {@link AgentIdentifier} that started a message
   * @param msgid the identifier of the message
   */
  public void stoppedMessage(AgentIdentifier from, String msgid);

}//interface MessagingServiceListener
