/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.messaging.model.EditableGroup;
import net.coalevo.messaging.model.MessagingException;
import net.coalevo.messaging.model.NoSuchGroupException;
import net.coalevo.presence.model.AgentIdentifierList;
import net.coalevo.presence.model.ServicePresenceProxy;
import net.coalevo.text.util.TagUtility;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Provides management of messaging groups,
 * permanent and temporary.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class GroupManager {

  private Marker m_LogMarker = MarkerFactory.getMarker(GroupManager.class.getName());
  protected MessagingStore m_Store;
  protected ServiceMediator m_Services;
  protected ServicePresenceProxy m_PresenceProxy;

  protected Set<String> m_GroupNames;
  protected Set<String> m_PermanentGroupNames;  
  protected Map<String, GroupImpl> m_ActiveGroups;

  public GroupManager(MessagingStore store) {
    m_Store = store;
  }//constructor

  public boolean isActive(String name) {
    return m_ActiveGroups.containsKey(name);
  }//isActive

  private void ensureActive(String name)
      throws MessagingException {
    if (!m_ActiveGroups.containsKey(name)) {
      throw new MessagingException(
          Activator.getBundleMessages().get("GroupManager.notactive")
      );
    }
  }//ensureActive

  public GroupImpl getGroup(String name)
      throws MessagingException, NoSuchGroupException {
    if (!m_GroupNames.contains(name)) {
      throw new NoSuchGroupException(name);
    } else {
      //try to get from memory (means active group)
      GroupImpl g = m_ActiveGroups.get(name);
      if (g == null) {
        //load from database, should be permanent
        g = loadGroup(name);
      }
      return g;
    }
  }//getGroup

  public Set<String> getGroupNames() {
    return Collections.unmodifiableSet(m_GroupNames);
  }//getGroupNames

  public boolean existsGroup(String name) {
    return m_GroupNames.contains(name);
  }//existsGroup

  public boolean isModerator(String gname, AgentIdentifier aid)
      throws MessagingException, NoSuchGroupException {
    return getGroup(gname).getModerator().equals(aid);
  }//isModerator

  public void ensurePresentMember(String gname, AgentIdentifier aid)
      throws MessagingException, NoSuchGroupException {

    if (!getGroup(gname).m_PresentMembers.contains(aid)) {
      throw new MessagingException();
    }
  }//ensurePresentMember

  public synchronized void maintain() {
    //1. collect all active members
    AIDSet allMembers = new AIDSet();
    for (String name : m_ActiveGroups.keySet()) {
      GroupImpl g = m_ActiveGroups.get(name);
      if (g.isPermanent()) {
        //store members for reference
        allMembers.addAll(g.getMembers());
      } else {
        m_GroupNames.remove(name);
      }
    }
    //clear active groups
    m_ActiveGroups.clear();
    //check presence of group members
    Activator.log().info(m_LogMarker, Activator.getBundleMessages().get("maintenance.do.repopulategroups"));
    m_PresenceProxy.getPresenceService().probePresence(m_PresenceProxy.getPresence(), allMembers);
  }//maintain

  //*** Group Handling ***//

  public void createPermanentGroup(EditableGroup eg)
      throws MessagingException {
    String gname = eg.getName();
    //1. Add group
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("group_name", gname);
    params.put("language", eg.getLanguage().getLanguage());
    params.put("infoformat", eg.getInfoFormat());
    params.put("info", eg.getInfo());
    params.put("tags", TagUtility.fromSet(eg.getTags()));
    params.put("moderator_id", eg.getModerator().getIdentifier());
    params.put("inviteonly", (eg.isInviteOnly()) ? "1" : "0");
    params.put("invitemoderated", (eg.isInviteModerated()) ? "1" : "0");
    params.put("created", Long.toString(eg.getCreated()));
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("createGroup", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "createPermanentGroup()::", ex);
      throw new MessagingException(Activator.getBundleMessages().get("GroupManager.permanent.createfailed"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }

    final GroupImpl g = (GroupImpl) eg.toGroup();
    final AgentIdentifier moderator = g.getModerator();

    //Add to active groups and groupname index
    m_GroupNames.add(gname);
    m_ActiveGroups.put(gname, g);
    m_PermanentGroupNames.add(gname);

    //Make Moderator a member
    g.m_Members.add(moderator);
    addMember(gname, moderator);

    //System.err.println(eg.toString());

    //Add Resource and probe moderator presence
    m_PresenceProxy.getPresence().addResource(gname);
    m_PresenceProxy.getPresenceService().probePresence(m_PresenceProxy.getPresence(), moderator);
  }//createPermanentGroup

  public void updatePermanentGroup(EditableGroup eg)
      throws MessagingException {
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("group_name", eg.getName());
    params.put("language", eg.getLanguage().getLanguage());
    params.put("infoformat", eg.getInfoFormat());
    params.put("info", eg.getInfo());
    params.put("tags", TagUtility.fromSet(eg.getTags()));
    params.put("moderator_id", eg.getModerator().getIdentifier());
    params.put("inviteonly", (eg.isInviteOnly()) ? "1" : "0");
    params.put("invitemoderated", (eg.isInviteModerated()) ? "1" : "0");
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("updateGroup", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "updatePermanentGroup()::", ex);
      throw new MessagingException(Activator.getBundleMessages().get("GroupManager.permanent.updatefailed"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
  }//updatePermanentGroup

  public void removePermanentGroup(String gname)
      throws MessagingException {

    HashMap<String, String> params = new HashMap<String, String>();
    params.put("group_name", gname);
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("removeGroup", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "removePermanentGroup()::", ex);
      throw new MessagingException(Activator.getBundleMessages().get("GroupManager.permanent.removefailed"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    m_GroupNames.remove(gname);
    m_PermanentGroupNames.remove(gname);
    m_ActiveGroups.remove(gname);
    m_PresenceProxy.getPresence().removeResource(gname);
  }//removePermanentGroup

  public GroupImpl createTemporaryGroup(String name, Locale l, AgentIdentifier by, String infoformat, String info,
                                        Set<String> tags, boolean inviteonly, boolean invitemod)
      throws MessagingException {

    GroupImpl g = new GroupImpl(name, l, infoformat, info, tags,
        by, false, inviteonly, invitemod, System.currentTimeMillis());

    //add creator
    g.m_PresentMembers.add(by);

    //add to names and groups
    m_GroupNames.add(name);
    m_ActiveGroups.put(name, g);
    m_PresenceProxy.getPresence().addResource(name);
    return g;
  }//createTemporaryGroup

  //*** Listings Handling ***//

  public Set<String> getGroupNames(boolean permanent) {
    Set<String> names;
    if(permanent) {
      names = new TreeSet<String>(m_PermanentGroupNames);
    } else {
      names = new TreeSet<String>();
      //copy all
      synchronized (m_GroupNames) {
        names.addAll(m_GroupNames);
      }
      //remove permanent
      names.removeAll(m_PermanentGroupNames);
    }
    return Collections.unmodifiableSet(names);
  }//getGroupNames

  /**
   * Returns the set of receivers in a specified group.
   * <p/>
   * If the sender parameter is specified, the sender is removed
   * from the receivers.
   *
   * @param gname  the group's name.
   * @param sender the sender if to be removed from the receivers.
   * @return a <tt>Set</tt> of {@link AgentIdentifier}s.
   * @throws MessagingException if the group is not active.
   */
  public Set<AgentIdentifier> getReceivers(String gname, AgentIdentifier sender)
      throws MessagingException {

    //1. Get Active Group only
    ensureActive(gname);
    GroupImpl g = getGroup(gname);

    Set<AgentIdentifier> r = new TreeSet<AgentIdentifier>(AID_COMPARATOR);
    r.addAll(g.m_PresentMembers);
    if (sender != null) {
      r.remove(sender);
    }
    return r;
  }//getReceivers

  /**
   * Lists the public active groups.
   *
   * @return a set of names of groups.
   */
  public Set<String> getPublicActive() {

    Set<String> groups = new TreeSet<String>(); //no dups and sorted naturally
    //No agent specified, list all
    synchronized (m_ActiveGroups) {
      for (Iterator<Map.Entry<String, GroupImpl>> iterator =
          m_ActiveGroups.entrySet().iterator(); iterator.hasNext();) {
        Map.Entry<String, GroupImpl> entry = iterator.next();
        GroupImpl g = entry.getValue();
        if (!g.isInviteOnly()) {
          groups.add(entry.getKey());
        }
      }
    }
    return groups;
  }//listPublicActive

  /**
   * Returns the names of the groups a given agent is invited to.
   *
   * @param aid the {@link AgentIdentifier}.
   * @return a <tt>Set</tt> of group names.
   * @throws MessagingException if the store access fails.
   */
  public Set<String> getInvitedTo(AgentIdentifier aid)
      throws MessagingException {

    //Without duplicates by definition
    Set<String> groups = new TreeSet<String>();

    //passive groups
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("agent_id", aid.getIdentifier());
    LibraryConnection lc = null;
    ResultSet rs = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("listAgentInvitations", params);
      while (rs.next()) {
        groups.add(rs.getString(1));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getInvitedTo()", ex);
      throw new MessagingException(Activator.getBundleMessages().get("GroupManager.permanent.listinvitationsfailed"), ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }

    //active groups
    synchronized (m_ActiveGroups) {
      for (Iterator<Map.Entry<String, GroupImpl>> iterator = m_ActiveGroups.entrySet().iterator(); iterator.hasNext();) {
        Map.Entry<String, GroupImpl> entry = iterator.next();
        GroupImpl g = entry.getValue();
        if (g.isInvited(aid)) {
          groups.add(entry.getKey());
        }
      }
    }
    return groups;
  }//getInvitedTo

  /**
   * Returns the memberships of a specified agent.
   * <p/>
   *
   * @param aid     the {@link AgentIdentifier}.
   * @param present flag that determines if the given agent is present
   *                (only active groups will be considered).
   * @return a <tt>Set</tt> of group names.
   * @throws MessagingException if the store access fails.
   */
  public Set<String> getMemberOf(AgentIdentifier aid, boolean present)
      throws MessagingException {

    Set<String> groups = new TreeSet<String>(); //no dups

    if (!present) {
      //Store
      HashMap<String, String> params = new HashMap<String, String>();
      params.put("agent_id", aid.getIdentifier());
      LibraryConnection lc = null;
      ResultSet rs = null;
      try {
        lc = m_Store.leaseConnection();
        rs = lc.executeQuery("listAgentMemberships", params);
        while (rs.next()) {
          groups.add(rs.getString(1));
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "getMemberOf()", ex);
        throw new MessagingException(Activator.getBundleMessages().get("GroupManager.permanent.listmembershipsfailed"), ex);
      } finally {
        SqlUtils.close(rs);
        m_Store.releaseConnection(lc);
      }
    }

    //active
    synchronized (m_ActiveGroups) {
      for (Iterator<Map.Entry<String, GroupImpl>> iterator = m_ActiveGroups.entrySet().iterator(); iterator.hasNext();) {
        Map.Entry<String, GroupImpl> entry = iterator.next();
        GroupImpl g = entry.getValue();
        if (g.isMember(aid)) {
          groups.add(entry.getKey());
        }
      }
    }
    return groups;
  }//getMemberOf

  /**
   * Returns all temporary groups the given agent is moderator of.
   *
   * @param aid the agent's {@link AgentIdentifier}.
   * @return a <tt>Set</tt> of <tt>Group</tt> instances.
   */
  public Set<GroupImpl> getModeratedTemporary(AgentIdentifier aid) {
    Set<GroupImpl> groups = new HashSet<GroupImpl>();
    synchronized (m_ActiveGroups) {
      for (Iterator<Map.Entry<String, GroupImpl>> iterator =
          m_ActiveGroups.entrySet().iterator(); iterator.hasNext();) {
        Map.Entry<String, GroupImpl> entry = iterator.next();
        GroupImpl g = entry.getValue();
        if (!g.isPermanent() && g.getModerator().equals(aid)) {
          groups.add(g);
        }
      }
    }
    return Collections.unmodifiableSet(groups);
  }//getModeratedTemporary

  /**
   * Returns all joinable groups for a given agent.
   *
   * @param aid the agent's {@link AgentIdentifier}.
   * @return a <tt>Set</tt> of group names.
   * @throws MessagingException if the operation fails.
   */
  public Set<String> getJoinableGroups(AgentIdentifier aid)
      throws MessagingException {

    Set<String> groups = new TreeSet<String>(); //no dups

    //Get from backend to ensure passive, permanent groups are covered.
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("agent_id", aid.getIdentifier());
    LibraryConnection lc = null;
    ResultSet rs = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("listAgentInvitations", params);
      while (rs.next()) {
        groups.add(rs.getString(1));
      }
      rs = lc.executeQuery("listPublicUnjoined", params);
      while (rs.next()) {
        groups.add(rs.getString(1));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getJoinableGroups()", ex);
      throw new MessagingException(Activator.getBundleMessages().get("GroupManager.permanent.listinvitationsfailed"), ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
    //active groups
    synchronized (m_ActiveGroups) {
      for (Iterator<Map.Entry<String, GroupImpl>> iterator = m_ActiveGroups.entrySet().iterator(); iterator.hasNext();) {
        Map.Entry<String, GroupImpl> entry = iterator.next();
        GroupImpl g = entry.getValue();
        if (!g.isMember(aid) && (!g.isInviteOnly() || g.isInvited(aid))) {
          groups.add(entry.getKey());
        }
      }
    }
    return groups;
  }//getJoinableGroups

  //*** Group Membership or Invitation State Change Mechanisms ***//

  public boolean invite(String gname, AgentIdentifier by, AgentIdentifier aid, boolean maymod)
      throws MessagingException, NoSuchGroupException {
    boolean notify = false;
    //1. Get Group
    GroupImpl g = getGroup(gname);

    //2. Check invitation
    if (g.isInviteOnly()) {
      //System.out.println("------> Invite only");
      if ((g.isInviteModerated() && !maymod)
          || (!g.isInviteModerated() && !g.m_PresentMembers.contains(by))) {
        throw new SecurityException();
      }
      if (g.isPermanent()) {
        if (g.m_Members.contains(aid)) {
          notify = false; //already member
        } else {
          if (g.m_Invited.add(aid)) {
            addInvite(gname, aid); //store
            notify = true;//notify if added to invitations
          }
        }
      } else {
        if (g.m_PresentMembers.contains(aid)) {
          notify = false;
        } else {
          notify = g.m_Invited.add(aid);
        }
      }
    } else {
      //public group handling
      if ((g.isPermanent() && g.m_Members.contains(aid))
          || ((!g.isPermanent() && g.m_PresentMembers.contains(aid)))
          || g.m_Invited.contains(aid)) {
        notify = false;
      } else {
        if (g.m_Invited.add(aid)) {
          if (g.isPermanent()) {
            addInvite(gname, aid); //store
          }
          notify = true;//notify if added to invitations
        }
      }
    }
    return notify;
  }//invite

  public boolean kick(String gname, AgentIdentifier by, AgentIdentifier aid, boolean maymod)
      throws MessagingException, NoSuchGroupException {
    boolean notify = false;
    //1. Get group
    GroupImpl g = getGroup(gname);

    if (!maymod || g.getModerator().equals(aid)) {
      throw new SecurityException();
    }
    //always remove from invited
    if (g.m_Invited.remove(aid)) {
      if (g.isPermanent()) {
        removeInvite(gname, aid);
      }
      return false;
    }

    if (g.isPermanent()) {
      if (!g.m_Members.contains(aid)) {
        notify = false; //already member
      } else {
        if (g.m_Members.remove(aid)) {
          removeMember(gname, aid);  //Store
          g.m_PresentMembers.remove(aid);
          notify = true;
        }
      }
    } else {
      notify = g.m_PresentMembers.remove(aid);
    }
    return notify;
  }//kick

  public boolean join(String gname, AgentIdentifier aid)
      throws MessagingException, NoSuchGroupException {
    //1. get group
    GroupImpl g = getGroup(gname);

    //2. If Member, no need to join
    if (g.getMembers().contains(aid)) {
      return false;
    }

    //3. Check invitation
    if (g.isInviteOnly() && !g.isInvited(aid)) {
      throw new SecurityException();
    }
    //Remove invitation
    if (g.isInvited(aid)) {
      g.m_Invited.remove(aid);
      removeInvite(gname, aid);
    }

    //4. Join
    if (g.isPermanent()) {
      addMember(gname, aid); //Backend store
      g.m_Members.add(aid);
      g.m_PresentMembers.add(aid); //presence is implicit
    } else {
      g.m_PresentMembers.add(aid);
    }
    return true;
  }//join

  public boolean leave(String gname, AgentIdentifier aid)
      throws MessagingException, NoSuchGroupException {
    //1. get group
    GroupImpl g = getGroup(gname);

    //2. If not Member, no need to leave
    if (!g.getMembers().contains(aid)) {
      return false;
    }

    //3. Leave
    if (g.isPermanent()) {
      removeMember(gname, aid); //Backend store
      g.m_Members.remove(aid);
      g.m_PresentMembers.remove(aid); //presence is implicit
      if (g.m_PresentMembers.isEmpty()) {
        //unload group
        m_ActiveGroups.remove(gname);
        if (g.m_Members.isEmpty()) {
          removePermanentGroup(g.getName());
        }
        return false;
      }
    } else {
      g.m_PresentMembers.remove(aid);
      if (g.m_PresentMembers.isEmpty()) {
        //unload group
        m_ActiveGroups.remove(gname);
        m_GroupNames.remove(gname);
        return false;
      }
    }

    return true;
  }//leave

  public void prepare() {
    m_Services = Activator.getServices();

    m_ActiveGroups = new ConcurrentHashMap<String, GroupImpl>();
    m_GroupNames = Collections.synchronizedSet(new HashSet<String>());

    //1. load group names from database
    LibraryConnection lc = null;
    ResultSet rs = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("listGroupNames", null);
      while (rs.next()) {
        m_GroupNames.add(rs.getString(1));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "prepare()", ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
    //copy permanent names
    m_PermanentGroupNames = new HashSet<String>(m_GroupNames);
    Activator.log().info(m_LogMarker, "Prepared " + m_GroupNames.toString());
  }//prepare

  public void preparePresence(ServicePresenceProxy p) {
    m_PresenceProxy = p;
    m_PresenceProxy.getPresence().setResources((String[]) m_GroupNames.toArray(new String[m_GroupNames.size()]));
  }//preparePresence

  //TODO: What happens to offline and inactive groups?
  protected void handleNoSubscription(final AgentIdentifier aid) {
    m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_PresenceProxy.getPresence().getAgent(), new Runnable() {
      public void run() {
        synchronized (m_ActiveGroups) {
          for (Iterator<Map.Entry<String, GroupImpl>> iterator = m_ActiveGroups.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<String, GroupImpl> entry = iterator.next();
            GroupImpl g = entry.getValue();
            if (g.isPermanent()) {
              if (g.m_Members.remove(aid)) {
                try {
                  removeMember(g.getName(), aid);
                } catch (MessagingException e) {
                  Activator.log().error(m_LogMarker, "handleNoSubscription()", e);
                }
                g.m_PresentMembers.remove(aid);
              }
              if (g.m_Members.isEmpty()) {
                try {
                  removePermanentGroup(g.getName());
                } catch (MessagingException e) {
                  Activator.log().error(m_LogMarker, "handleNoSubscription()", e);
                }
              }
            } else {
              g.m_PresentMembers.remove(aid);
              if (g.m_PresentMembers.isEmpty()) {
                iterator.remove();
              }
            }
          }
        }
        //Delete all occurrences of the aid
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("agent_id", aid.getIdentifier());
        LibraryConnection lc = null;
        try {
          lc = m_Store.leaseConnection();
          lc.executeUpdate("removeAgentMemberships", params);
          lc.executeUpdate("removeAgentInvitations", params);
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, "handleNoSubscription()", ex);
        } finally {
          m_Store.releaseConnection(lc);
        }
      }//run
    }//runnable
    );//execute
  }//handleNoSubscription

  private synchronized GroupImpl loadGroup(String gname)
      throws MessagingException {

    //Ensure we just load a group once
    if (m_ActiveGroups.containsKey(gname)) {
      return m_ActiveGroups.get(gname);
    }

    HashMap<String, String> params = new HashMap<String, String>();
    params.put("group_name", gname);

    LibraryConnection lc = null;
    ResultSet rs = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("getGroup", params);
      final GroupImpl g;
      if (rs.next()) {
        g = new GroupImpl(
            gname,                                //name
            new Locale(rs.getString(2)),          //lang
            rs.getString(3),                      //infoformat
            rs.getString(4),                      //info
            TagUtility.toSet(rs.getString(5)),    //tags
            new AgentIdentifier(rs.getString(6)), //moderator
            true,                                 //permanent
            rs.getShort(7) == 1,                  //inviteonly
            rs.getShort(8) == 1,                  //invitemoderated
            rs.getLong(9)                         //created
        );

      } else {
        throw new NoSuchGroupException(gname);
      }
      SqlUtils.close(rs);
      rs = lc.executeQuery("listGroupInvitations", params);
      while (rs.next()) {
        g.m_Invited.add(m_Store.getAgentIdentifier(rs.getString(1)));
      }
      SqlUtils.close(rs);
      rs = lc.executeQuery("listGroupMembers", params);
      while (rs.next()) {
        g.m_Members.add(m_Store.getAgentIdentifier(rs.getString(1)));
      }
      //add it
      m_ActiveGroups.put(gname, g);
      Activator.log().info(m_LogMarker, "Loaded " + g.toString());

      //Start probing task
      m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_PresenceProxy.getPresence().getAgent(), new Runnable() {
        public void run() {
          for (AgentIdentifier aid : g.m_Members) {
            m_PresenceProxy.getPresenceService().probePresence(m_PresenceProxy.getPresence(), aid);
          }
        }//run
      }//runnable
      );//execute
      return g;
    } catch (Exception ex) {
      Activator.log().error("loadGroup()", ex);
      throw new MessagingException(Activator.getBundleMessages().get("GroupManager.permanent.getfailed"), ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
  }//loadGroup

  protected void addInvite(String gname, AgentIdentifier aid)
      throws MessagingException {
    //1. Add group
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("group_name", gname);
    params.put("agent_id", aid.getIdentifier());
    params.put("issued", Long.toString(System.currentTimeMillis()));

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("addGroupInvitation", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "addInvite()", ex);
      throw new MessagingException(Activator.getBundleMessages().get("GroupManager.invite.addfailed"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
  }//addInvite

  protected void removeInvite(String gname, AgentIdentifier aid)
      throws MessagingException {
    //1. Add group
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("group_name", gname);
    params.put("agent_id", aid.getIdentifier());

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("removeGroupInvitation", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "removeInvite()", ex);
      throw new MessagingException(Activator.getBundleMessages().get("GroupManager.invite.addfailed"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
  }//removeInvite

  protected void addMember(String gname, AgentIdentifier aid)
      throws MessagingException {
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("group_name", gname);
    params.put("agent_id", aid.getIdentifier());

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("addGroupMember", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "addMember()", ex);
      throw new MessagingException(Activator.getBundleMessages().get("GroupManager.member.addfailed"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
  }//addInvite

  protected void removeMember(String gname, AgentIdentifier aid)
      throws MessagingException {
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("group_name", gname);
    params.put("agent_id", aid.getIdentifier());

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("removeGroupMember", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "removeMember()", ex);
      throw new MessagingException(Activator.getBundleMessages().get("GroupManager.member.removefailed"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
  }//removeMember

  Comparator<AgentIdentifier> AID_COMPARATOR = new Comparator<AgentIdentifier>() {

    public int compare(AgentIdentifier aid1, AgentIdentifier aid2) {
      String a1 = aid1.getIdentifier();
      String a2 = aid2.getIdentifier();
      return a1.compareTo(a2);
    }//compare

  };

  class AIDSet implements AgentIdentifierList {

    private HashSet<AgentIdentifier> m_Set;

    public AIDSet() {
      m_Set = new HashSet<AgentIdentifier>();
    }//constructor

    public boolean add(AgentIdentifier aid) {
      return m_Set.add(aid);
    }//add

    public void addAll(Collection<AgentIdentifier> c) {
      m_Set.addAll(c);
    }//addAll

    public boolean remove(AgentIdentifier aid) {
      return m_Set.remove(aid);
    }//remove

    public void clear() {
      m_Set.clear();
    }//clear

    public boolean contains(AgentIdentifier aid) {
      return m_Set.contains(aid);
    }//contains

    public Iterator iterator() {
      return m_Set.iterator();
    }//iterator

    public boolean isEmpty() {
      return m_Set.isEmpty();
    }//isEmpty

  }//class AIDSet

}//class GroupManager
