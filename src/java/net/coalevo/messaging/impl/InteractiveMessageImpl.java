/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.messaging.model.BaseEventInfo;
import net.coalevo.messaging.model.EditableInteractiveMessage;
import net.coalevo.messaging.model.EventInfo;
import net.coalevo.messaging.model.InteractiveMessageType;
import org.apache.commons.collections.FastHashMap;

import java.io.*;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/**
 * Implements {@link EditableInteractiveMessage}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class InteractiveMessageImpl
    implements EditableInteractiveMessage, Serializable {

  static final long serialVersionUID = -5539264028292109836L;

  protected String m_Identifier;
  protected AgentIdentifier m_To;
  protected AgentIdentifier m_From;
  protected String m_Thread;
  protected Map<Locale, String> m_Subjects;
  protected Map<Locale, String> m_Bodies;
  protected Locale m_Locale;
  protected boolean m_Broadcast;
  protected boolean m_Confirmation;
  protected InteractiveMessageType m_Type;
  protected String m_FormattingHint;
  protected boolean m_Multicast;
  protected long m_Created = System.currentTimeMillis();

  public InteractiveMessageImpl(String id) {
    m_Identifier = id;
    m_Locale = Locale.getDefault();
    m_Broadcast = false;
    m_Confirmation = false;
    m_Bodies = (Map<Locale, String>) new FastHashMap();
    m_Subjects = (Map<Locale, String>) new FastHashMap();
  }//constructor

  public InteractiveMessageImpl(String id, boolean b) {
    m_Identifier = id;
    m_Locale = Locale.getDefault();
    m_Broadcast = false;
    m_Confirmation = false;
    m_Bodies = (Map<Locale, String>) new FastHashMap();
    m_Subjects = (Map<Locale, String>) new FastHashMap();
    m_Broadcast = b;
  }//constructor

  public AgentIdentifier getFrom() {
    return m_From;
  }//getFrom

  public void setFrom(AgentIdentifier aid) {
    m_From = aid;
  }//setFrom

  public AgentIdentifier getTo() {
    return m_To;
  }//getTo

  public void setTo(AgentIdentifier aid) {
    m_To = aid;
  }//setTo

  public void setType(InteractiveMessageType t) {
    m_Type = t;
  }//setType

  public InteractiveMessageType getType() {
    return m_Type;
  }//getType

  public String getThread() {
    return m_Thread;
  }//setThread

  public void setThread(String str) {
    m_Thread = str;
  }//setThread

  public boolean hasThread() {
    return (m_Thread != null && m_Thread.length() > 0);
  }//hasThread

  public boolean isThread(String thread) {
    return (hasThread() && m_Thread.equals(thread));
  }//isThread

  public String getSubject(Locale l) {
    return (String) m_Subjects.get(l);
  }//getSubject

  public String getSubject() {
    if (m_Subjects.containsKey(m_Locale)) {
      return m_Subjects.get(m_Locale);
    } else if (m_Subjects.size() > 0) {
      return m_Subjects.entrySet().iterator().next().getValue();
    } else {
      return null;
    }
  }//getSubject

  public Iterator<Map.Entry<Locale, String>> getSubjects() {
    return m_Subjects.entrySet().iterator();
  }//getSubjects

  public void setSubject(String s) {
    m_Subjects.put(m_Locale, s);
  }//setSubject

  public void setSubject(Locale l, String s) {
    m_Subjects.put(l, s);
  }//setSubject

  public void removeSubject(Locale l) {
    m_Subjects.remove(l);
  }//removeSubject

  public boolean hasSubject(Locale l) {
    return m_Subjects.containsKey(l);
  }//hasSubject

  public boolean hasSubject() {
    return m_Subjects.containsKey(m_Locale);
  }//hasSubject

  public boolean hasSubjects() {
    return m_Subjects.size() > 0;
  }//hasSubjects

  public String getBody(Locale l) {
    return (String) m_Bodies.get(l);
  }//getBody

  public String getBody() {
    if (m_Bodies.containsKey(m_Locale)) {
      return m_Bodies.get(m_Locale);
    } else if (m_Bodies.size() > 0) {
      return m_Bodies.entrySet().iterator().next().getValue();
    } else {
      return null;
    }
  }//getBody

  public Iterator<Map.Entry<Locale, String>> getBodies() {
    return m_Bodies.entrySet().iterator();
  }//getBodies

  public void setBody(String body) {
    m_Bodies.put(m_Locale, body);
  }//setBody

  public void setBody(Locale l, String b) {
    m_Bodies.put(l, b);
  }//setBody

  public void removeBody(Locale l) {
    m_Bodies.remove(l);
  }//removeBody

  public boolean hasBody(Locale l) {
    return m_Bodies.containsKey(l);
  }//hasBody

  public boolean hasBody() {
    return m_Bodies.containsKey(m_Locale);
  }//hasBody

  public boolean hasBodies() {
    return m_Bodies.size() > 0;
  }//hasBodies

  public boolean isConfirmationRequired() {
    return m_Confirmation;
  }//isConfirmationRequired

  public boolean isBroadcast() {
    return m_Broadcast;
  }//isBroadcast

  public boolean isMulticast() {
    return m_Multicast;
  }//isMulticast

  public long getCreated() {
    return m_Created;
  }//getCreated

  public Locale getLocale() {
    return m_Locale;
  }//getLocale

  public boolean hasLocale() {
    return m_Locale != null;
  }//hasLocale

  public EventInfo getEventInfo() {
    return new EventInfoImpl();
  }//getEventInfo

  public EventInfo getEventInfo(boolean b) {
    return new EventInfoImpl(b);
  }//getEventInfo

  public String getFormattingHint() {
    return m_FormattingHint;
  }//getFormattingHint

  public void setFormattingHint(String hint) {
    m_FormattingHint = hint;
  }//setFormattingHint

  public boolean hasFormattingHint() {
    return m_FormattingHint != null && m_FormattingHint.length() > 0;
  }//hasFormattingHint

  public void setConfirmationRequired(boolean b) {
    m_Confirmation = b;
  }//setConfirmationRequired

  public void setBroadcast(boolean b) {
    m_Broadcast = b;
  }//setBroadcast

  public void clear() {
    m_To = null;
    m_From = null;
    m_Thread = null;
    m_Subjects.clear();
    m_Bodies.clear();
    m_Locale = Locale.getDefault();
    m_Broadcast = false;
    m_Confirmation = false;
    m_FormattingHint = null;
  }//clear

  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    InteractiveMessageImpl that = (InteractiveMessageImpl) o;

    if (m_Identifier != null ? !m_Identifier.equals(that.m_Identifier) : that.m_Identifier != null) return false;

    return true;
  }//equals

  public int hashCode() {
    return (m_Identifier != null ? m_Identifier.hashCode() : 0);
  }//hashCode

  class EventInfoImpl
      extends BaseEventInfo {

    public EventInfoImpl() {
      super(InteractiveMessageImpl.this.m_Identifier,
          InteractiveMessageImpl.this.m_From,
          InteractiveMessageImpl.this.m_To
      );
    }//constructor

    public EventInfoImpl(boolean b) {
      super(InteractiveMessageImpl.this.m_Identifier,
          InteractiveMessageImpl.this.m_From,
          InteractiveMessageImpl.this.m_To,
          b);
    }//constructor

  }//class EventInfoImpl

  public static void main(String[] args) {
    InteractiveMessageImpl msg = new InteractiveMessageImpl("xyz123");
    msg.setSubject(Locale.ENGLISH, "Test Me");
    msg.setSubject(Locale.GERMAN, "Teste Mich");
    msg.setBody(Locale.ENGLISH, "Test text body");
    msg.setBody(Locale.GERMAN, "Test text body");
    msg.setConfirmationRequired(true);

    ByteArrayOutputStream bout = new ByteArrayOutputStream();
    try {
      ObjectOutputStream out = new ObjectOutputStream(bout);
      out.writeObject(msg);

      System.out.println("Encoded:" + bout.size());

      ObjectInputStream oin = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()));
      InteractiveMessageImpl msg2 = (InteractiveMessageImpl) oin.readObject();
      System.out.println("Decoded: ID=" + msg2.getIdentifier());
      System.out.println(msg2.getSubject(Locale.ENGLISH));
      System.out.println(msg2.getSubject(Locale.GERMAN));
      System.out.println(msg2.getBody(Locale.ENGLISH));
      System.out.println(msg2.getBody(Locale.GERMAN));
      System.out.println("Confirmation " + ((msg2.isConfirmationRequired()) ? "required" : "not required"));
    } catch (Exception e) {

      e.printStackTrace();
    }

  }

}//class InteractiveMessageImpl
