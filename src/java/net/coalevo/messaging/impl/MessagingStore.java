/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.impl;

import net.coalevo.datasource.service.DataSourceService;
import net.coalevo.foundation.model.*;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.messaging.service.MessagingConfiguration;
import net.coalevo.security.service.SecurityService;
import net.coalevo.security.model.ServiceAgentProxy;
import net.coalevo.userdata.service.UserdataService;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.osgi.framework.BundleContext;
import org.slamb.axamol.library.Library;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.sql.DataSource;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Provides a backend store to persist groups.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MessagingStore
    implements ConfigurationUpdateHandler {

  private Marker m_LogMarker = MarkerFactory.getMarker(MessagingStore.class.getName());
  private Library m_SQLLibrary;
  private DataSource m_DataSource;
  private GenericObjectPool m_ConnectionPool;
  private boolean m_New = false;
  private Messages m_BundleMessages;

  //Concurrency handling backup/restore
  private AtomicInteger m_NumLeased = new AtomicInteger(0);
  private CountDownLatch m_LeaseLatch;
  private CountDownLatch m_RestoreLatch;

  //Managers & Caches
  private AgentIdentifierInstanceCache m_AgentIdentifierCache;

  //Maintenance configuration
  private int m_OfflineMsgExpiry = 7;       //days
  private int m_InvitationExpiry = 14;      //days

  public MessagingStore() {
  }//constructor

  public boolean isNew() {
    return m_New;
  }//isNew

  /**
   * Lease a connection to the underlying database.
   *
   * @return a {@link LibraryConnection} instance.
   * @throws Exception if the connection pool fails or a connection cannot be created.
   */
  public LibraryConnection leaseConnection()
      throws Exception {
    //If a lease latch is set, wait for countdown
    if (m_LeaseLatch != null) {
      m_LeaseLatch.await();
    }
    LibraryConnection lc = (LibraryConnection) m_ConnectionPool.borrowObject();
    m_NumLeased.addAndGet(1);
    return lc;
  }//leaseConnection

  public void releaseConnection(LibraryConnection lc) {
    try {
      m_ConnectionPool.returnObject(lc);
      if (m_NumLeased.decrementAndGet() == 0 && m_RestoreLatch != null) {
        m_RestoreLatch.countDown();
      }
    } catch (Exception e) {
      Activator.log().error(m_LogMarker, "releaseConnection()", e);
    }
  }//releaseConnection

  private synchronized void prepareDataSource() {
    LibraryConnection lc = null;
    try {
      lc = leaseConnection();
      //1. check select
      try {
        lc.executeQuery("existsSchema", null);
      } catch (SQLException ex) {
        m_New = true;
        createSchema(lc);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "prepareDataSource()", ex);
    } finally {
      releaseConnection(lc);
    }
  }//prepareDataSource

  private synchronized boolean createSchema(LibraryConnection lc) {
    try {
      lc = leaseConnection();
      //1. Create schema, tables and index
      lc.executeCreate("createMessagingSchema");
      lc.executeCreate("createGroupTable");
      lc.executeCreate("createMemberTable");
      lc.executeCreate("createInvitationTable");
      lc.executeCreate("createOfflineMessageTable");
      return true;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "createDatabase()", ex);
    } finally {
      releaseConnection(lc);
    }
    return false;
  }//createDatabase


  public boolean activate(BundleContext bc) {
    m_BundleMessages = Activator.getBundleMessages();
    //1. Configure from persistent configuration
    String ds = "embedded";
    int cpoolsize = 3;
    int aidcachesize = 50;


    ConfigurationMediator cm = Activator.getServices().getConfigMediator();
    MetaTypeDictionary mtd = cm.getConfiguration();

    //Retrieve config
    try {
      ds = mtd.getString(MessagingConfiguration.DATA_SOURCE_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingStore.activation.configexception", "attribute", MessagingConfiguration.DATA_SOURCE_KEY),
          ex
      );
    }
    try {
      cpoolsize = mtd.getInteger(MessagingConfiguration.CONNECTION_POOLSIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingStore.activation.configexception", "attribute", MessagingConfiguration.CONNECTION_POOLSIZE_KEY),
          ex
      );
    }
    try {
      aidcachesize = mtd.getInteger(MessagingConfiguration.AGENTIDENTIFIERS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingStore.activation.configexception", "attribute", MessagingConfiguration.AGENTIDENTIFIERS_CACHESIZE_KEY),
          ex
      );
    }
    GenericObjectPool.Config poolcfg = new GenericObjectPool.Config();
    poolcfg.maxActive = cpoolsize;
    poolcfg.maxIdle = cpoolsize;
    poolcfg.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_BLOCK;
    poolcfg.testOnBorrow = true;
    m_ConnectionPool = new GenericObjectPool(new ConnectionFactory(), poolcfg);

    //prepare SQL library and datasource
    try {
      m_SQLLibrary =
          new Library(MessagingStore.class, "net/coalevo/messaging/impl/messagingstore-sql.xml");
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "activate()", ex);
      return false;
    }
    DataSourceService dss = Activator.getServices().getDataSourceService(ServiceMediator.WAIT_UNLIMITED);
    try {
      m_DataSource = dss.waitForDataSource(ds, -1);
    } catch (NoSuchElementException nse) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingStore.activation.datasource"),
          nse
      );
      return false;
    }
    //prepare store
    prepareDataSource();
    Activator.log().info(m_LogMarker,
        m_BundleMessages.get("MessagingStore.database.info", "source", m_DataSource.toString())
    );
    //Create Managers
    m_AgentIdentifierCache = new AgentIdentifierInstanceCache(aidcachesize);
    //add handler config updates
    cm.addUpdateHandler(this);
    return true;
  }//activate

  public synchronized boolean deactivate() {

    //1. close all leased connections
    if (m_ConnectionPool != null) {
      try {
        m_ConnectionPool.close();
      } catch (Exception e) {
        Activator.log().error(m_LogMarker, "deactivate()", e);
      }
    }

    m_DataSource = null;
    m_SQLLibrary = null;
    m_ConnectionPool = null;
    m_BundleMessages = null;
    return true;
  }//deactivate

  public void update(MetaTypeDictionary mtd) {
    //1. Configure from persistent configuration
    int cpoolsize = 5;
    int aidcachesize = 50;

    //Retrieve config
    try {
      cpoolsize = mtd.getInteger(MessagingConfiguration.CONNECTION_POOLSIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingStore.activation.configexception", "attribute", MessagingConfiguration.CONNECTION_POOLSIZE_KEY),
          ex
      );
    }
    try {
      aidcachesize = mtd.getInteger(MessagingConfiguration.AGENTIDENTIFIERS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingStore.activation.configexception", "attribute", MessagingConfiguration.AGENTIDENTIFIERS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      m_OfflineMsgExpiry = mtd.getInteger(MessagingConfiguration.OFFLINEMSG_EXPIRY_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingStore.activation.configexception", "attribute", MessagingConfiguration.OFFLINEMSG_EXPIRY_KEY),
          ex
      );
    }
    try {
      m_InvitationExpiry = mtd.getInteger(MessagingConfiguration.INVITATION_EXPIRY_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingStore.activation.configexception", "attribute", MessagingConfiguration.INVITATION_EXPIRY_KEY),
          ex
      );
    }
    //Update settings
    m_ConnectionPool.setMaxActive(cpoolsize);
    m_ConnectionPool.setMaxIdle(cpoolsize);
    m_AgentIdentifierCache.setCeiling(aidcachesize);
  }//update

  //*** Caches and Managers ***//

  public AgentIdentifier getAgentIdentifier(String aid) {
    return m_AgentIdentifierCache.get(aid);
  }//getAgentIdentifier


  /**
   * Backups the database of userdata while running.
   * The database will be frozen for writes, but no interruption
   * for reads is required.
   *
   * @param f   the directory to write the backup to.
   * @param tag a possible tag for the backup.
   * @throws SecurityException
   * @throws BackupException   g
   */
  public void backup(File f, String tag)
      throws BackupException {

  }//doBackup

  public void restore(File f, String tag)
      throws RestoreException {

  }//restore

  public void maintain(ServiceAgentProxy agent) throws MaintenanceException {

    UserdataService uds = Activator.getServices().getUserdataService(ServiceMediator.NO_WAIT);
    if (uds == null) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("maintenance.error.service", "service", "UserdataService"));
      return;
    }
    SecurityService ss = agent.getSecurityService();
    if (ss == null) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("maintenance.error.service", "service", "SecurityService"));
      return;
    }

    //Database maintainance
    ResultSet rs = null;
    HashMap<String, String> params = new HashMap<String, String>();

    LibraryConnection lc = null;
    // Expiry in millis
    long expired;

    try {

      lc = leaseConnection();
      //1. Purge offline messages stored for deleted agents
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.store.offlinemsgstart"));
      cleanupForRemovedAgents(rs,lc,uds,ss,agent.getAuthenticPeer(),params,
          "getAgentsWithOfflineMessages","removeOfflineMessages");
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.store.offlinemsgstop"));

      //2. Purge expired offline messages
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.store.offlinemsgexpiry"));
      params.clear();
      expired = System.currentTimeMillis() - (m_OfflineMsgExpiry * 86400000);
      params.put("expiry", Long.toString(expired));
      lc.executeUpdate("removeExpiredOfflineMessages", params);

      //3. Purge memberships stored for deleted agents
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.store.membershipstart"));
      cleanupForRemovedAgents(rs,lc,uds,ss,agent.getAuthenticPeer(),params,
          "getAgentsWithMemberships","removeAgentMemberships");
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.store.membershipstop"));

      //4. Purge invitations stored for deleted agents
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.store.invitationstart"));
      cleanupForRemovedAgents(rs,lc,uds,ss,agent.getAuthenticPeer(),params,
          "getAgentsWithInvitations","removeAgentInvitations");
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.store.invitationstop"));

      //5. Purge expired invitations
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.store.invitationexpiry"));
      expired = System.currentTimeMillis() - (m_InvitationExpiry * 86400000);
      params.clear();
      params.put("expiry", Long.toString(expired));
      lc.executeUpdate("removeExpiredInvitations", params);

    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("maintenance.error.store"), ex);
    } finally {
      SqlUtils.close(rs);
      releaseConnection(lc);
    }

    //clear caches
    Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.store.caches"));
    m_AgentIdentifierCache.clear();
  }//maintain

  private void cleanupForRemovedAgents(ResultSet rs,
                                       LibraryConnection lc,
                                       UserdataService uds,
                                       SecurityService ss,
                                       ServiceAgent agent,
                                       HashMap<String, String> params,
                                       String listQuery,
                                       String removeUpdate) throws SQLException {
    rs = lc.executeQuery(listQuery, null);
    while (rs.next()) {
      String aid = rs.getString(1);
      AgentIdentifier a_id = new AgentIdentifier(aid);
      if (!uds.isUserdataAvailable(agent, a_id)) {
        //Check that it isn't a service by role
        //HACK: See CSB-9
        Set roles = ss.getAgentRolesByName(agent, a_id);
        boolean isService = false;
        for (Object o : roles) {
          if (o.equals("Service")) {
            isService = true;
            break;
          }
        }
        //If it is a service, ignore it
        if (isService) {
          continue;
        }
        //Purge if no userdata available
        params.clear();
        params.put("agent_id", aid);
        try {
          lc.executeUpdate(removeUpdate, params);
          Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.store.removedagent", "aid", params.get("agent_id")));
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, m_BundleMessages.get("maintenance.error.removedagent", "aid", params.get("agent_id")));
        }
      }
    }

  }//cleanupForRemovedAgents

  private class ConnectionFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      Connection c = m_DataSource.getConnection();
      c.setAutoCommit(true);
      return new LibraryConnection(m_SQLLibrary, c);
    }//makeObject

    public boolean validateObject(Object obj) {
      final LibraryConnection lc = (LibraryConnection) obj;
      try {
        lc.executeQuery("validate", null);
      } catch (Exception ex) {
        return false;
      }
      return true;
    }//validateObject

    public void destroyObject(Object obj) {
      final LibraryConnection lc = (LibraryConnection) obj;
      if (!lc.isClosed()) {
        SqlUtils.close(lc);
      }
    }//destroyObject

  }//ConnectionFactory

}//class MessagingStore
