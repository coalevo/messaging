/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.impl;

import net.coalevo.foundation.model.Maintainable;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.util.BundleConfiguration;
import net.coalevo.logging.model.LogProxy;
import net.coalevo.messaging.service.GroupMessagingService;
import net.coalevo.messaging.service.MessagingConfiguration;
import net.coalevo.messaging.service.MessagingService;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a <tt>BundleActivator</tt> implementation for
 * the messaging service bundle.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator
    implements BundleActivator {

  private static Marker c_LogMarker;
  private static LogProxy c_Log;

  private static ServiceMediator c_Services;
  private static Messages c_BundleMessages;
  private static BundleConfiguration c_BundleConfiguration;
  private static MessagingRouterManagerImpl c_MERMgr;
  private static MessagingStore c_MessagingStore;
  private static MessagingServiceImpl c_MessagingService;
  private static GroupMessagingServiceImpl c_GroupMessagingService;

  private Thread m_StartThread;

  public void start(final BundleContext bundleContext)
      throws Exception {

    if(m_StartThread!=null && m_StartThread.isAlive()) {
      throw new Exception();
    }    
    m_StartThread = new Thread(
        new Runnable() {
          public void run() {
            try {
              //1. Log
              c_LogMarker = MarkerFactory.getMarker(Activator.class.getName());
              c_Log = new LogProxy();
              c_Log.activate(bundleContext);

              //2. Services
              c_Services = new ServiceMediator();
              c_Services.activate(bundleContext);

              //3. Bundle Messages
              c_BundleMessages =
                  c_Services.getMessageResourceService(ServiceMediator.WAIT_UNLIMITED)
                      .getBundleMessages(bundleContext.getBundle());

              //4. Bundle Configuration
              c_BundleConfiguration = new BundleConfiguration(MessagingConfiguration.class.getName());
              c_BundleConfiguration.activate(bundleContext);
              c_Services.setConfigMediator(c_BundleConfiguration.getConfigurationMediator());

              //5. Messaging Store
              c_MessagingStore = new MessagingStore();
              c_MessagingStore.activate(bundleContext);

              //6. Messaging Services
              c_MessagingService = new MessagingServiceImpl(c_MessagingStore);
              c_Services.setMessagingService(c_MessagingService);
              if (!c_MessagingService.activate(bundleContext)) {
                log().error(c_LogMarker, c_BundleMessages.get("Activator.activation.exception", "service", "MessagingService"));
                return;
              }
              String[] classes = {MessagingService.class.getName(), Maintainable.class.getName()};
              bundleContext.registerService(
                  classes,
                  c_MessagingService,
                  null);
              log().debug(c_LogMarker, c_BundleMessages.get("Activator.activation.service", "service", "MessagingService"));

              c_GroupMessagingService = new GroupMessagingServiceImpl(c_MessagingStore);
              if (!c_GroupMessagingService.activate(bundleContext)) {
                log().error(c_LogMarker, c_BundleMessages.get("Activator.activation.exception", "service", "GroupMessagingService"));
                return;
              }
              classes[0] = GroupMessagingService.class.getName();
              bundleContext.registerService(
                  classes,
                  c_GroupMessagingService,
                  null);
              log().debug(c_LogMarker, c_BundleMessages.get("Activator.activation.service", "service", "GroupMessagingService"));

              //7. Message Routing (for distribution)
              c_MERMgr = new MessagingRouterManagerImpl();
              c_MERMgr.activate(bundleContext);
              c_Services.setMessagingRouterManager(c_MERMgr);

            } catch (Exception ex) {
              log().error(c_LogMarker, "start(BundleContext)", ex);
            }
          }//run
        }//Runnable
    );//Thread
    m_StartThread.setContextClassLoader(getClass().getClassLoader());
    m_StartThread.start();
  }//start

  public void stop(BundleContext bundleContext)
      throws Exception {

    //wait start
    if (m_StartThread != null && m_StartThread.isAlive()) {
      m_StartThread.join();
      m_StartThread = null;
    }

    try {
      if (c_GroupMessagingService != null) {
        c_GroupMessagingService.deactivate();
        c_GroupMessagingService = null;
      }
    } catch (Exception ex) {
      log().error("stop()", ex);
    }

    try {
      if (c_MessagingService != null) {
        c_MessagingService.deactivate();
        c_MessagingService = null;
      }
    } catch (Exception ex) {
      log().error("stop()", ex);
    }

    try {
      if (c_MessagingStore != null) {
        c_MessagingStore.deactivate();
        c_MessagingStore = null;
      }
    } catch (Exception ex) {
      log().error("stop()", ex);
    }

    try {
      if (c_BundleConfiguration != null) {
        c_BundleConfiguration.deactivate();
        c_BundleConfiguration = null;
      }
    } catch (Exception ex) {
      log().error("stop()", ex);
    }

    try {
      if (c_MERMgr != null) {
        c_MERMgr.deactivate();
        c_MERMgr = null;
      }
    } catch (Exception ex) {
      log().error("stop()", ex);
    }

    if (c_Services != null) {
      c_Services.deactivate();
      c_Services = null;
    }

    if (c_Log != null) {
      c_Log.deactivate();
      c_Log = null;
    }

    c_LogMarker = null;
    c_BundleMessages = null;
  }//stop

  public static ServiceMediator getServices() {
    return c_Services;
  }//getServices

  public static Messages getBundleMessages() {
    return c_BundleMessages;
  }//getBundleMessages

  /**
   * Return the bundles logger.
   *
   * @return the <tt>Logger</tt>.
   */
  public static Logger log() {
    return c_Log;
  }//log

}//class Activator
