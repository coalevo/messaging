/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.messaging.model.*;
import net.coalevo.messaging.service.MessagingConfiguration;
import net.coalevo.presence.model.Presence;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Provides a base implementation messaging related event management.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
abstract class MessagingEventManager
    implements ConfigurationUpdateHandler {

  private Marker m_LogMarker = MarkerFactory.getMarker(MessagingEventManager.class.getName());
  protected GenericObjectPool m_MessageDeliveryPool;
  protected GenericObjectPool m_EventNotifyPool;
  protected GenericObjectPool m_BroadcasterPool;
  protected GenericObjectPool m_ConfirmerPool;
  protected Messages m_BundleMessages = Activator.getBundleMessages();
  protected ServiceMediator m_Services = Activator.getServices();

  public MessagingEventManager() {
    //5. Prepare pools
    GenericObjectPool.Config poolConfig = new GenericObjectPool.Config();
    poolConfig.maxActive = 25;
    poolConfig.maxIdle = 5;
    poolConfig.minIdle = 5;
    //block and wait
    poolConfig.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_GROW;
    poolConfig.maxWait = -1;
    //test on borrow
    poolConfig.testOnBorrow = true;
    m_MessageDeliveryPool = new GenericObjectPool(new MessageDeliveryFactory(), poolConfig);
    m_EventNotifyPool = new GenericObjectPool(new EventNotifyFactory(), poolConfig);
    m_BroadcasterPool = new GenericObjectPool(new BroadcasterFactory(), poolConfig);
    m_ConfirmerPool = new GenericObjectPool(new ConfirmationFactory(), poolConfig);
  }//constructor


  public abstract MessagingServiceListener getMessagingServiceListener(AgentIdentifier aid);

  public abstract Iterator getRegistered();

  public abstract boolean isPresenceAvailable(AgentIdentifier aid);

  protected abstract void addConfirmable(EventInfo ei);

  public void update(MetaTypeDictionary mtd) {
    int maxactive = 25;
    int maxidle = 10;
    int minidle = 5;
    int maxwait = -1;
    String exhaustAction = MessagingConfiguration.POOLEXHAUST_BLOCK;

    //Message Delivery Pool
    try {
      maxactive = mtd.getInteger(MessagingConfiguration.MSGDELIVERYPOOL_MAXACTIVE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.MSGDELIVERYPOOL_MAXACTIVE_KEY),
          ex
      );
    }
    try {
      maxidle = mtd.getInteger(MessagingConfiguration.MSGDELIVERYPOOL_MAXIDLE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.MSGDELIVERYPOOL_MAXIDLE_KEY),
          ex
      );
    }
    try {
      minidle = mtd.getInteger(MessagingConfiguration.MSGDELIVERYPOOL_MINIDLE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.MSGDELIVERYPOOL_MINIDLE_KEY),
          ex
      );
    }
    try {
      maxwait = mtd.getInteger(MessagingConfiguration.MSGDELIVERYPOOL_MAXWAIT_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.MSGDELIVERYPOOL_MAXWAIT_KEY),
          ex
      );
    }
    try {
      exhaustAction = mtd.getString(MessagingConfiguration.MSGDELIVERYPOOL_EXHAUSTACTION_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.MSGDELIVERYPOOL_EXHAUSTACTION_KEY),
          ex
      );
    }
    m_MessageDeliveryPool.setMaxActive(maxactive);
    m_MessageDeliveryPool.setMaxIdle(maxidle);
    m_MessageDeliveryPool.setMinIdle(minidle);
    m_MessageDeliveryPool.setMaxWait(maxwait);
    if (exhaustAction.equals(MessagingConfiguration.POOLEXHAUST_BLOCK)) {
      m_MessageDeliveryPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_BLOCK);
    } else if (exhaustAction.equals(MessagingConfiguration.POOLEXHAUST_FAIL)) {
      m_MessageDeliveryPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_FAIL);
    } else if (exhaustAction.equals(MessagingConfiguration.POOLEXHAUST_GROW)) {
      m_MessageDeliveryPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_GROW);
    }

    //Event Notification Pool
    try {
      maxactive = mtd.getInteger(MessagingConfiguration.EVENTNOTIFYPOOL_MAXACTIVE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.EVENTNOTIFYPOOL_MAXACTIVE_KEY),
          ex
      );
    }
    try {
      maxidle = mtd.getInteger(MessagingConfiguration.EVENTNOTIFYPOOL_MAXIDLE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.EVENTNOTIFYPOOL_MAXIDLE_KEY),
          ex
      );
    }
    try {
      minidle = mtd.getInteger(MessagingConfiguration.EVENTNOTIFYPOOL_MINIDLE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.EVENTNOTIFYPOOL_MINIDLE_KEY),
          ex
      );
    }
    try {
      maxwait = mtd.getInteger(MessagingConfiguration.EVENTNOTIFYPOOL_MAXWAIT_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.EVENTNOTIFYPOOL_MAXWAIT_KEY),
          ex
      );
    }
    try {
      exhaustAction = mtd.getString(MessagingConfiguration.EVENTNOTIFYPOOL_EXHAUSTACTION_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.EVENTNOTIFYPOOL_EXHAUSTACTION_KEY),
          ex
      );
    }
    m_EventNotifyPool.setMaxActive(maxactive);
    m_EventNotifyPool.setMaxIdle(maxidle);
    m_EventNotifyPool.setMinIdle(minidle);
    m_EventNotifyPool.setMaxWait(maxwait);
    if (exhaustAction.equals(MessagingConfiguration.POOLEXHAUST_BLOCK)) {
      m_EventNotifyPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_BLOCK);
    } else if (exhaustAction.equals(MessagingConfiguration.POOLEXHAUST_FAIL)) {
      m_EventNotifyPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_FAIL);
    } else if (exhaustAction.equals(MessagingConfiguration.POOLEXHAUST_GROW)) {
      m_EventNotifyPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_GROW);
    }

    //Broadcaster Pool
    try {
      maxactive = mtd.getInteger(MessagingConfiguration.BROADCASTPOOL_MAXACTIVE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.BROADCASTPOOL_MAXACTIVE_KEY),
          ex
      );
    }
    try {
      maxidle = mtd.getInteger(MessagingConfiguration.BROADCASTPOOL_MAXIDLE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.BROADCASTPOOL_MAXIDLE_KEY),
          ex
      );
    }
    try {
      minidle = mtd.getInteger(MessagingConfiguration.BROADCASTPOOL_MINIDLE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.BROADCASTPOOL_MINIDLE_KEY),
          ex
      );
    }
    try {
      maxwait = mtd.getInteger(MessagingConfiguration.BROADCASTPOOL_MAXWAIT_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.BROADCASTPOOL_MAXWAIT_KEY),
          ex
      );
    }
    try {
      exhaustAction = mtd.getString(MessagingConfiguration.BROADCASTPOOL_EXHAUSTACTION_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.BROADCASTPOOL_EXHAUSTACTION_KEY),
          ex
      );
    }
    m_BroadcasterPool.setMaxActive(maxactive);
    m_BroadcasterPool.setMaxIdle(maxidle);
    m_BroadcasterPool.setMinIdle(minidle);
    m_BroadcasterPool.setMaxWait(maxwait);
    if (exhaustAction.equals(MessagingConfiguration.POOLEXHAUST_BLOCK)) {
      m_BroadcasterPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_BLOCK);
    } else if (exhaustAction.equals(MessagingConfiguration.POOLEXHAUST_FAIL)) {
      m_BroadcasterPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_FAIL);
    } else if (exhaustAction.equals(MessagingConfiguration.POOLEXHAUST_GROW)) {
      m_BroadcasterPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_GROW);
    }

    //Confirmation Pool
    try {
      maxactive = mtd.getInteger(MessagingConfiguration.CONFIRMATIONPOOL_MAXACTIVE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.CONFIRMATIONPOOL_MAXACTIVE_KEY),
          ex
      );
    }
    try {
      maxidle = mtd.getInteger(MessagingConfiguration.CONFIRMATIONPOOL_MAXIDLE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.CONFIRMATIONPOOL_MAXIDLE_KEY),
          ex
      );
    }
    try {
      minidle = mtd.getInteger(MessagingConfiguration.CONFIRMATIONPOOL_MINIDLE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.CONFIRMATIONPOOL_MINIDLE_KEY),
          ex
      );
    }
    try {
      maxwait = mtd.getInteger(MessagingConfiguration.CONFIRMATIONPOOL_MAXWAIT_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.CONFIRMATIONPOOL_MAXWAIT_KEY),
          ex
      );
    }
    try {
      exhaustAction = mtd.getString(MessagingConfiguration.CONFIRMATIONPOOL_EXHAUSTACTION_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingEventManager.activation.configexception", "attribute", MessagingConfiguration.CONFIRMATIONPOOL_EXHAUSTACTION_KEY),
          ex
      );
    }
    m_ConfirmerPool.setMaxActive(maxactive);
    m_ConfirmerPool.setMaxIdle(maxidle);
    m_ConfirmerPool.setMinIdle(minidle);
    m_ConfirmerPool.setMaxWait(maxwait);
    if (exhaustAction.equals(MessagingConfiguration.POOLEXHAUST_BLOCK)) {
      m_ConfirmerPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_BLOCK);
    } else if (exhaustAction.equals(MessagingConfiguration.POOLEXHAUST_FAIL)) {
      m_ConfirmerPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_FAIL);
    } else if (exhaustAction.equals(MessagingConfiguration.POOLEXHAUST_GROW)) {
      m_ConfirmerPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_GROW);
    }


  }//update


  class MessageDeliveryFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      return new MessageDelivery(m_MessageDeliveryPool);
    }//makeObject

    public void passivateObject(Object o)
        throws Exception {
      if (o instanceof MessageDelivery) {
        ((MessageDelivery) o).reset();
      }
    }//passivateObject

  }//inner class MessageDeliveryFactory

  class EventNotifyFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      return new EventNotify(m_EventNotifyPool);
    }//makeObject


    public void passivateObject(Object o)
        throws Exception {
      if (o instanceof EventNotify) {
        ((EventNotify) o).reset();
      }
    }//passivateObject


  }//inner class EventNotifyFactory

  class BroadcasterFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      return new Broadcaster(m_BroadcasterPool);
    }//makeObject


    public void passivateObject(Object o)
        throws Exception {
      if (o instanceof Broadcaster) {
        ((Broadcaster) o).reset();
      }
    }//passivateObject

  }//inner class BroadcasterFactory

  class ConfirmationFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      return new Confirmer(m_ConfirmerPool);
    }//makeObject


    public void passivateObject(Object o)
        throws Exception {
      if (o instanceof Confirmer) {
        ((Confirmer) o).reset();
      }
    }//passivateObject

  }//inner class ConfirmerFactory

  class MessageDelivery
      implements Runnable {

    protected GenericObjectPool m_Pool;
    protected Message m_Message;

    public MessageDelivery(GenericObjectPool pool) {
      m_Pool = pool;
    }//constructor

    public void setMessage(Message m) {
      m_Message = m;
    }//setMessage

    public void reset() {
      m_Message = null;
    }//reset

    public void run() {
      try {
        AgentIdentifier to = m_Message.getTo();
        if (to.isLocal()) {
          deliverLocal();
        } else {
          route();
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "MessageDelivery::run()", ex);
      } finally {
        try {
          m_Pool.returnObject(this);
        } catch (Exception e) {
          Activator.log().error(m_LogMarker, "MessageDelivery::run()", e);
        }
      }
    }//run

    protected void deliverLocal() {
      MessagingServiceListener msl = getMessagingServiceListener(m_Message.getTo());
      if (msl != null) {
        if (m_Message instanceof Request) {
          msl.received((Request) m_Message);
        } else if (m_Message instanceof Response) {
          msl.received((Response) m_Message);
        } else if (m_Message instanceof InteractiveMessage) {
          msl.received((InteractiveMessage) m_Message);
        }
      } else {
        msl = getMessagingServiceListener(m_Message.getFrom());
        if (msl == null) {
          Activator.log().error(m_LogMarker, "MessageDelivery::deliverLocal()::MessagingServiceListener");
        } else {
          msl.failedDelivery(m_Message);
        }
      }
    }//deliverLocal

    private void route() {
      for (Iterator iter = m_Services.getMessagingRouterManager().listAvailable(); iter.hasNext();) {
        MessagingRouter mr = (MessagingRouter) iter.next();
        //check the domain.
        if (mr.routes(m_Message)) {
          mr.route(m_Message);
        }

      }
    }//route

  }//MessageDelivery

  class EventNotify
      implements Runnable {

    private GenericObjectPool m_Pool;
    private boolean m_Started;
    private AgentIdentifier m_From;
    private AgentIdentifier m_To;
    private String m_Id;

    public EventNotify(GenericObjectPool pool) {
      m_Pool = pool;
    }//constructor

    public void setStart(AgentIdentifier from, AgentIdentifier to, String id) {
      m_Started = true;
      m_From = from;
      m_To = to;
      m_Id = id;
    }//set

    public void setStop(AgentIdentifier from, AgentIdentifier to, String id) {
      m_Started = false;
      m_From = from;
      m_To = to;
      m_Id = id;
    }//set

    public void reset() {
      m_From = null;
      m_To = null;
      m_Id = null;
    }//reset

    public void run() {
      try {
        MessagingServiceListener msl = getMessagingServiceListener(m_To);
        if (msl != null) {
          if (m_Started) {
            msl.startedMessage(m_From, m_Id);
          } else {
            msl.stoppedMessage(m_From, m_Id);
          }
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "EventNotify::run()", ex);
      } finally {
        try {
          m_Pool.returnObject(this);
        } catch (Exception e) {
          Activator.log().error(m_LogMarker, "EventNotify::run()", e);
        }
      }
    }//run

  }//EventNotify

  class Broadcaster
      implements Runnable {

    private InteractiveMessageImpl m_Message;
    private GenericObjectPool m_Pool;
    private Set<AgentIdentifier> m_Receivers;

    public Broadcaster(GenericObjectPool pool) {
      m_Pool = pool;
    }//constructor

    public void setMessage(InteractiveMessageImpl m) {
      m_Message = m;
    }//setMessage

    public void setReceivers(Set<AgentIdentifier> l) {
      m_Receivers = l;
    }//setReceivers

    public void reset() {
      m_Receivers = null;
      m_Message = null;
    }//reset

    public void run() {
      try {
        if (m_Receivers != null) {
          //Multicast to available and listening
          for (Iterator iter = m_Receivers.iterator(); iter.hasNext();) {
            AgentIdentifier aid = (AgentIdentifier) iter.next();
            m_Message.setTo(aid);
            if (m_Message.isConfirmationRequired()) {
              addConfirmable(m_Message.getEventInfo(true));
            }
            if (aid.isLocal()) {
              deliverLocal();
            } else {
              route();
            }
          }
        } else {
          //Broadcast all available
          for (Iterator iter = getRegistered(); iter.hasNext();) {
            Map.Entry entry = (Map.Entry) iter.next();
            Presence p = (Presence) entry.getKey();
            MessagingServiceListener l = (MessagingServiceListener) entry.getValue();
            AgentIdentifier aid = p.getAgentIdentifier();
            m_Message.setTo(aid);
            if (m_Message.isConfirmationRequired()) {
              addConfirmable(m_Message.getEventInfo());
            }
            l.received(m_Message);
          }
          //broadcast to all routers
          for (Iterator iter = m_Services.getMessagingRouterManager().listAvailable(); iter.hasNext();) {
            MessagingRouter mr = (MessagingRouter) iter.next();
            //check the domain.
            if (mr.routes(m_Message)) {
              mr.routeBroadcast(m_Message);
            }
          }
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "Broadcaster::run()", ex);
      } finally {
        try {
          m_Pool.returnObject(this);
        } catch (Exception e) {
          Activator.log().error(m_LogMarker, "Broadcaster::run()", e);
        }
      }
    }//run

    protected void deliverLocal() {
      MessagingServiceListener msl = getMessagingServiceListener(m_Message.getTo());
      if (msl != null) {
        msl.received((InteractiveMessage) m_Message);
      } else {
        //Inform sender?
        msl = getMessagingServiceListener(m_Message.getFrom());
        if (msl == null) {
          Activator.log().error(m_LogMarker, "MessageDelivery::deliverLocal()::MessagingServiceListener");
        } else {
          msl.failedDelivery(m_Message);
        }
      }
    }//deliverLocal

    private void route() {
      for (Iterator iter = m_Services.getMessagingRouterManager().listAvailable(); iter.hasNext();) {
        MessagingRouter mr = (MessagingRouter) iter.next();
        //check the domain.
        if (mr.routes(m_Message)) {
          mr.route(m_Message);
        }
      }
    }//route

  }//Broadcaster

  class Confirmer implements Runnable {

    private GenericObjectPool m_Pool;
    private EventInfo m_Info;
    private long m_Timestamp;

    public Confirmer(GenericObjectPool pool) {
      m_Pool = pool;
    }//constructor

    public long set(EventInfo info) {
      m_Info = info;
      m_Timestamp = System.currentTimeMillis();
      return m_Timestamp;
    }//setMessage

    public void reset() {
      m_Info = null;
      m_Timestamp = -1;
    }//reset

    public void run() {
      try {
        //confirm delivery if there is no exception
        MessagingServiceListener msl = getMessagingServiceListener(m_Info.getFrom());
        if (msl == null) {
          Activator.log().error(m_LogMarker, "MessageDelivery::deliverLocal()::MessagingServiceListener");
        } else {
          msl.confirmed(m_Info, m_Timestamp);
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "Confirmer::run()", ex);
      } finally {
        try {
          m_Pool.returnObject(this);
        } catch (Exception e) {
          Activator.log().error(m_LogMarker, "Confirmer::run()", e);
        }
      }
    }//run
  }//Broadcaster

  /*
   TODO: Routing of incoming messages to local listeners
  */

}//class MessagingEventManager
