/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.impl;

import net.coalevo.messaging.model.MessagingRouter;
import net.coalevo.messaging.model.MessagingRouterManager;
import org.osgi.framework.*;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Implements a whiteboard {@link MessagingRouterManager} for
 * {@link MessagingRouter} instances registered with the container.
 * <p/>
 * Will pick up
 * </p>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MessagingRouterManagerImpl
    implements MessagingRouterManager {

  private Marker m_LogMarker = MarkerFactory.getMarker(MessagingRouterManagerImpl.class.getName());
  private BundleContext m_BundleContext;
  private Map<String, MessagingRouter> m_MessagingRouters;

  public MessagingRouterManagerImpl() {
    m_MessagingRouters = new ConcurrentHashMap<String, MessagingRouter>();
  }//MessagingRouterManagerImpl

  /**
   * Activates this <tt>MessagingRouterManagerImpl</tt>.
   * The logic will automatically register all {@link MessagingRouter}
   * class objects, whether registered before or after the activation
   * (i.e. white board model implementation).
   *
   * @param bc the <tt>BundleContext</tt>.
   */
  public void activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    //prepare listener
    MessagingRouterListener pl = new MessagingRouterListener();
    //prepare the filter
    String filter = "(objectclass=" + MessagingRouter.class.getName() + ")";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(pl, filter);
      //ensure that already registered ShellService instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        pl.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      Activator.log().error(m_LogMarker, "activate()", ex);
    }
  }//activate

  /**
   * Deactivates this <tt>ShellServiceManagerImpl</tt>.
   * The logic will remove the listener and release all
   * references.
   */
  public void deactivate() {

    //null out the references
    m_MessagingRouters.clear();

    m_MessagingRouters = null;
    m_BundleContext = null;
  }//deactivate

  public boolean register(MessagingRouter er) {
    String dom = er.getDomain();
    if (m_MessagingRouters.containsKey(dom)) {
      return false;
    } else {
      m_MessagingRouters.put(dom, er);
      Activator.log().info(m_LogMarker, "Registered MessagingRouter for domain " + dom);
      return true;
    }
  }//register

  public boolean unregister(String dom) {
    if (!m_MessagingRouters.containsKey(dom)) {
      return false;
    } else {
      m_MessagingRouters.remove(dom);
      Activator.log().info(m_LogMarker, "Unregistered MessagingRouter for domain " + dom);
      return true;
    }
  }//unregister

  public MessagingRouter get(String dom) {
    Object o = m_MessagingRouters.get(dom);
    if (o != null) {
      return (MessagingRouter) o;
    } else {
      throw new NoSuchElementException(dom);
    }
  }//get

  public boolean isAvailable(String dom) {
    return m_MessagingRouters.containsKey(dom);
  }//isAvailable

  public Iterator<String> listAvailable() {
    return m_MessagingRouters.keySet().iterator();
  }//listAvailable


  private class MessagingRouterListener
      implements ServiceListener {

    private Marker m_LogMarker = MarkerFactory.getMarker(MessagingRouterListener.class.getName());

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            Activator.log().error(m_LogMarker, "ServiceListener:serviceChanged:registered:null");
          } else if (!(o instanceof MessagingRouter)) {
            Activator.log().error(m_LogMarker, "ServiceListener:serviceChanged:registered:Reference not a MessagingRouter instance.");
          } else {
            register((MessagingRouter) o);
          }
          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            Activator.log().error(m_LogMarker, "ServiceListener:serviceChanged:unregistering:null");
          } else if (!(o instanceof MessagingRouter)) {
            Activator.log().error(m_LogMarker, "ServiceListener:serviceChanged:unregistering:Reference not a MessagingRouter instance.");
          } else {
            unregister(((MessagingRouter) o).getDomain());
          }
          break;
      }
    }
  }//inner class MessagingRouterListener


}//class MessagingRouterManagerImpl
