/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.impl;

import java.util.Iterator;
import java.util.LinkedHashSet;

/**
 * Provides a set that will allow a maximum amount of
 * members, dropping old entries in a FIFO manner.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class FixedSizeSet<T>
    extends LinkedHashSet<T> {

  private int m_MaxSize;
  private EntryExpungeHandler<T> m_ExpungeHandler;

  public FixedSizeSet(int size) {
    super();
    m_MaxSize = size;
  }//constructor

  public FixedSizeSet(int size, EntryExpungeHandler<T> handler) {
    super();
    m_MaxSize = size;
    m_ExpungeHandler = handler;
  }//constructor

  public EntryExpungeHandler<T> getEntryExpungeHandler() {
    return m_ExpungeHandler;
  }//getEntryExpungeHandler

  public void setEntryExpungeHandler(EntryExpungeHandler<T> expungeHandler) {
    m_ExpungeHandler = expungeHandler;
  }//setEntryExpungeHandler

  public int getMaxSize() {
    return m_MaxSize;
  }//getMaxSize

  public void setMaxSize(int maxSize) {
    m_MaxSize = maxSize;
  }//setMaxSize

  public boolean add(T o) {
    boolean b = super.add(o);
    //delete the eldest if required
    if (this.size() > m_MaxSize) {
      removeEldest();
    }
    return b;
  }//add

  private void removeEldest() {
    if (this.size() > m_MaxSize) {
      Iterator<T> iter = this.iterator();
      T o = iter.next();
      iter.remove();
      if (m_ExpungeHandler != null) {
        m_ExpungeHandler.expunged(o);
      }
    }
  }//removeEldest

  public static interface EntryExpungeHandler<T> {

    public void expunged(T o);
  }//EntryExpungeHandler

}//class FixedSizeSet
