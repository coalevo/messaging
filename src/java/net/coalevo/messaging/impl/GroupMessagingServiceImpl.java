/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.impl;

import net.coalevo.foundation.model.*;
import net.coalevo.messaging.model.*;
import net.coalevo.messaging.service.GroupMessagingService;
import net.coalevo.presence.model.*;
import net.coalevo.security.model.NoSuchActionException;
import net.coalevo.security.model.PolicyProxy;
import net.coalevo.security.model.ServiceAgentProxy;
import net.coalevo.text.util.TagUtility;
import org.osgi.framework.BundleContext;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.*;

/**
 * Provides an implementation of {@link GroupMessagingService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class GroupMessagingServiceImpl
    extends BaseService
    implements GroupMessagingService, Maintainable {

  private Marker m_LogMarker = MarkerFactory.getMarker(GroupMessagingServiceImpl.class.getName());

  //Agent, Presence and Policy
  private ServiceMediator m_Services;

  //Security Context
  private ServiceAgentProxy m_ServiceAgentProxy;
  private PolicyProxy m_PolicyProxy;
  private ServicePresenceProxy m_ServicePresenceProxy;

  private MessagingStore m_Store;
  private GroupManager m_GroupManager;
  private Messages m_BundleMessages;

  private Map<String, EditableGroup> m_CreatePGTracker;
  private Map<String, EditableGroup> m_UpdatePGTracker;
  private Map<String, InteractiveMessage> m_GroupMessageTracker;


  public GroupMessagingServiceImpl(MessagingStore ds) {
    super(GroupMessagingService.class.getName(), ACTIONS);
    m_Store = ds;
  }//GroupMessagingServiceImpl

  public boolean activate(BundleContext bc) {
    m_Services = Activator.getServices();
    m_BundleMessages = Activator.getBundleMessages();

    //1. Authenticate ourself
    m_ServiceAgentProxy = new ServiceAgentProxy(this, Activator.log());
    m_ServiceAgentProxy.activate(bc);

    //2. Policy
    m_PolicyProxy = new PolicyProxy(
        getIdentifier(),
        "COALEVO-INF/security/GroupMessagingService-policy.xml",
        m_ServiceAgentProxy,
        Activator.log()
    );
    m_PolicyProxy.activate(bc);

    //Prepare GroupManager
    m_GroupManager = new GroupManager(m_Store);
    m_GroupManager.prepare();

    //Presence Handling
    m_ServicePresenceProxy = new ServicePresenceProxy(
        new PresenceServiceListenerImpl(),
        m_ServiceAgentProxy,
        Activator.log());
    m_ServicePresenceProxy.activate(bc);
    //MessagingHandling
    m_Services.getMessagingService().register(
        m_ServicePresenceProxy.getPresence(),
        new MessagingServiceListenerAdapter() {
        }
    );
    m_GroupManager.preparePresence(m_ServicePresenceProxy);

    m_CreatePGTracker = new WeakHashMap<String, EditableGroup>();
    m_UpdatePGTracker = new WeakHashMap<String, EditableGroup>();
    m_GroupMessageTracker = new WeakHashMap<String, InteractiveMessage>();
    return true;
  }//activate

  public boolean deactivate() {

    //1. Trackers
    if (m_CreatePGTracker != null) {
      m_CreatePGTracker.clear();
    }
    if (m_UpdatePGTracker != null) {
      m_UpdatePGTracker.clear();
    }
    if (m_GroupMessageTracker != null) {
      m_GroupMessageTracker.clear();
    }

    //2. Messaging & Presence
    m_Services.getMessagingService().unregister(m_ServicePresenceProxy.getPresence());

    if (m_ServicePresenceProxy != null) {
      m_ServicePresenceProxy.deactivate();
      m_ServicePresenceProxy = null;
    }


    //3. Security
    if (m_PolicyProxy != null) {
      m_PolicyProxy.deactivate();
      m_PolicyProxy = null;
    }
    if (m_ServiceAgentProxy != null) {
      m_ServiceAgentProxy.deactivate();
      m_ServiceAgentProxy = null;
    }

    //4. Null all refs
    m_Store = null;
    m_GroupManager = null;
    m_BundleMessages = null;
    m_CreatePGTracker = null;
    m_UpdatePGTracker = null;
    m_GroupMessageTracker = null;
    m_Services = null;
    return true;
  }//deactivate

  //*** Permanent Group Management ***//

  public EditableGroup beginCreatePermanentGroup(Agent caller, String name)
      throws SecurityException, MessagingException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_PERMANENT_GROUP);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_PERMANENT_GROUP.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    //2. Ensure there is no duplicate name
    if (m_GroupManager.existsGroup(name)) {
      throw new MessagingException(m_BundleMessages.get("GroupMessagingServiceImpl.permanent.create"));
    }
    //3. Create a Group and return the editable part
    EditableGroup eg = new GroupImpl(name, true).toEditableGroup();
    m_CreatePGTracker.put(name, eg);
    return eg;
  }//beginCreatePermanentGroup

  public EditableGroup beginUpdatePermanentGroup(Agent caller, String name)
      throws SecurityException, MessagingException, NoSuchGroupException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), UPDATE_PERMANENT_GROUP);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", UPDATE_PERMANENT_GROUP.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    //2. Retrieve group
    EditableGroup eg = m_GroupManager.getGroup(name).toEditableGroup();
    m_UpdatePGTracker.put(name, eg);
    return eg;
  }//beginUpdatePermanentGroup

  public void commitPermanentGroup(Agent caller, EditableGroup eg)
      throws SecurityException, MessagingException {

    m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    String gname = eg.getName();
    ensureSubscriptionTo(eg.getModerator());

    if (m_CreatePGTracker.containsKey(gname)) {
      m_GroupManager.createPermanentGroup(eg);
      ensureSubscriptionTo(eg.getModerator());
    } else if (m_UpdatePGTracker.containsKey(gname)) {
      m_GroupManager.updatePermanentGroup(eg);
    } else {
      throw new IllegalStateException(gname);
    }
  }//commitPermanentGroup

  public void cancelPermanentGroupTransaction(Agent caller, EditableGroup eg) {
    m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    m_CreatePGTracker.remove(eg.getName());
    m_UpdatePGTracker.remove(eg.getName());
  }//cancelPermanentGroupTransaction

  public void removePermanentGroup(Agent caller, String gname)
      throws SecurityException, NoSuchGroupException, MessagingException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), REMOVE_PERMANENT_GROUP);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", REMOVE_PERMANENT_GROUP.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    //2. Existance check
    if (!m_GroupManager.existsGroup(gname)) {
      throw new NoSuchGroupException(gname);
    }

    m_GroupManager.removePermanentGroup(gname);
  }//removePermanentGroup

  public Group createTemporaryGroup(Agent caller, String name, Locale l, String infoformat, String info, String tags, boolean inviteonly, boolean invitemod)
      throws SecurityException, MessagingException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_TEMPORARY_GROUP);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_TEMPORARY_GROUP.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    //2. Ensure there is no duplicate name
    if (m_GroupManager.existsGroup(name)) {
      throw new MessagingException(m_BundleMessages.get("GroupMessagingServiceImpl.permanent.create"));
    }

    //3. create and return group
    Group g = m_GroupManager.createTemporaryGroup(name, l,
        caller.getAgentIdentifier(), infoformat, info, TagUtility.toSet(tags),
        inviteonly, invitemod);
    ensureSubscriptionTo(caller.getAgentIdentifier());
    return g;
  }//createTemporaryGroup

  public boolean existsGroup(Agent caller, String gname)
      throws SecurityException {
    //is technically the same as being allowed to list the groups.
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), EXISTS_GROUP);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", EXISTS_GROUP.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    return m_GroupManager.existsGroup(gname);
  }//existsGroup

  public Group getGroup(Agent caller, String gname)
      throws SecurityException, MessagingException {
    final AgentIdentifier aid = caller.getAgentIdentifier();

    //1. check authenticity (is implicit for standard, but not for group member checks.
    m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    //2. Get Group
    GroupImpl g = (GroupImpl) m_GroupManager.getGroup(gname);
    if (g.isInviteOnly()) {
      //3. Invite only, check membership first:
      if (g.isMember(aid) || g.isInvited(aid)) {
        return g;
      } else {
        try {
          m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_GROUP);
        } catch (NoSuchActionException ex) {
          Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_GROUP.getIdentifier(), "caller", caller.getIdentifier()), ex);
        }
        //fall through means that no security exception was thrown
        return g;
      }
    } else {
      return g;
    }
  }//getGroup

  public Set<String> listGroups(Agent caller, boolean permanent)
      throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), LIST_GROUPS);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", LIST_GROUPS.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_GroupManager.getGroupNames(permanent);
  }//listGroups

  public Set<String> listGroupInvitations(Agent caller)
      throws SecurityException, MessagingException {

    m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    return m_GroupManager.getInvitedTo(caller.getAgentIdentifier());
  }//listGroupInvitations

  public Set<String> listPublicGroups(Agent caller)
      throws SecurityException, MessagingException {

    m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    return m_GroupManager.getPublicActive();
  }//listPublicGroups

  public Set<String> listGroupMemberships(Agent caller, AgentIdentifier aid)
      throws SecurityException, MessagingException {
    AgentIdentifier caid = caller.getAgentIdentifier();
    if (!caid.equals(aid)) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), LIST_MEMBERSHIPS);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", LIST_MEMBERSHIPS.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }
    return m_GroupManager.getMemberOf(
        aid,
        m_ServicePresenceProxy.getPresenceService().isAvailable(
            m_ServiceAgentProxy.getAuthenticPeer(), aid)
    );
  }//listGroupMemberships

  public Set<String> listGroupInvitations(Agent caller, AgentIdentifier aid)
      throws SecurityException, MessagingException {
    AgentIdentifier caid = caller.getAgentIdentifier();
    if (!caid.equals(aid)) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), LIST_INVITATIONS);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", LIST_INVITATIONS.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }
    return m_GroupManager.getInvitedTo(aid);
  }//listGroupInvitations

  public Set<String> listJoinableGroups(Presence p)
      throws SecurityException, MessagingException {

    return m_GroupManager.getJoinableGroups(p.getAgentIdentifier());
  }//listJoinableGroups

  public boolean isPermanentGroup(Agent caller, String group)
      throws SecurityException, MessagingException, NoSuchGroupException {
    m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    return m_GroupManager.getGroup(group).isPermanent();
  }//isPermanentGroup

  //*** Membership Management ***//

  private boolean mayModerate(String gname, Agent caller)
      throws MessagingException {
    //Policy check to resolve true or false
    if (!m_GroupManager.isModerator(gname, caller.getAgentIdentifier())) {
      //check policy
      return m_PolicyProxy.checkAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), MODERATE_GROUP);
    } else {
      return true;
    }
  }//mayModerate

  private void ensureSubscriptionTo(final AgentIdentifier to) {
    m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_ServiceAgentProxy.getAuthenticPeer(), new Runnable() {
      public void run() {
        try {
          AgentIdentifierList subscribedto = m_ServicePresenceProxy.getPresence().getSubscriptionsTo();
          List pendingreq = m_ServicePresenceProxy.getPresenceService().listPendingRequests(m_ServicePresenceProxy.getPresence());
          if (!subscribedto.contains(to) && !pendingreq.contains(to)) {
            m_ServicePresenceProxy.getPresenceService().requestSubscriptionTo(m_ServicePresenceProxy.getPresence(), to);
          }
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, "ensureSubscriptionTo()", ex);
        }
      }//run
    });
  }//ensureSubscriptionTo

  private EditableInteractiveMessage createNotificationMessage()
      throws MessagingException {
    return m_Services.getMessagingService().create(
        m_ServicePresenceProxy.getPresence(),
        InteractiveMessageTypes.NOTIFICATION,
        m_Services.getUIDGeneratorService(ServiceMediator.NO_WAIT).getUID());
  }//createNotificationMessage

  public void inviteToGroup(final Agent caller, String gname, final AgentIdentifier aid)
      throws SecurityException, MessagingException, NoSuchGroupException {

    //2. Existance check
    final GroupImpl g = m_GroupManager.getGroup(gname);

    if (m_GroupManager.invite(gname, caller.getAgentIdentifier(), aid, mayModerate(gname, caller))) {
      ensureSubscriptionTo(aid);
      //notify
      final Set<AgentIdentifier> receivers = m_GroupManager.getReceivers(gname, null);
      m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_ServiceAgentProxy.getAuthenticPeer(), new Runnable() {
        public void run() {
          try {

            //Notification of invited
            EditableInteractiveMessage im = createNotificationMessage();
            Locale lang = g.getLanguage();
            im.setTo(aid);
            im.setConfirmationRequired(false);
            im.setThread(g.getName());
            im.setSubject(LANG_MACHINE, "invite");
            im.setSubject(lang,
                m_BundleMessages.get(
                    lang,
                    "GroupMessagingService.notification.invite.subject"
                )
            );
            im.setBody(lang,
                m_BundleMessages.get(
                    lang,
                    "GroupMessagingService.notification.invite.whobody",
                    "by",
                    caller.getAgentIdentifier().getIdentifier()
                )
            );
            m_Services.getMessagingService().send(m_ServicePresenceProxy.getPresence(), im);

            //Notification of group members
            im = createNotificationMessage();
            im.setConfirmationRequired(false);
            im.setThread(g.getName());
            im.setSubject(LANG_MACHINE, "invited");
            im.setSubject(lang,
                m_BundleMessages.get(
                    lang,
                    "GroupMessagingService.notification.invite.subject"
                )
            );
            im.setBody(lang,
                m_BundleMessages.get(
                    lang,
                    "GroupMessagingService.notification.invite.body",
                    "by",
                    caller.getAgentIdentifier().getIdentifier(),
                    "who",
                    aid.getIdentifier()
                )
            );
            //3. Multicast (will also run through the execution service)
            m_Services.getMessagingService().multicast(m_ServicePresenceProxy.getPresence(), im, receivers);
          } catch (Exception ex) {
            Activator.log().error(m_LogMarker, "inviteToGroup()", ex);
          }
        }//run
      });
    }
  }//inviteToGroup

  public void kickFromGroup(final Agent caller, String gname, final AgentIdentifier aid)
      throws SecurityException, MessagingException, NoSuchGroupException {

    //2. Existance check
    final GroupImpl g = m_GroupManager.getGroup(gname);

    if (m_GroupManager.kick(gname, caller.getAgentIdentifier(), aid, mayModerate(gname, caller))) {
      //notify
      final Set<AgentIdentifier> receivers = m_GroupManager.getReceivers(gname, null);
      m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_ServiceAgentProxy.getAuthenticPeer(), new Runnable() {
        public void run() {
          try {

            //Notification of kicked
            EditableInteractiveMessage im = createNotificationMessage();
            Locale lang = g.getLanguage();
            im.setTo(aid);
            im.setConfirmationRequired(false);
            im.setThread(g.getName());
            im.setSubject(LANG_MACHINE, "kick");
            im.setSubject(lang,
                m_BundleMessages.get(
                    lang,
                    "GroupMessagingService.notification.kick.subject"
                )
            );
            im.setBody(lang,
                m_BundleMessages.get(
                    lang,
                    "GroupMessagingService.notification.kick.whobody",
                    "by",
                    caller.getAgentIdentifier().getIdentifier()
                )
            );
            m_Services.getMessagingService().send(m_ServicePresenceProxy.getPresence(), im);

            //Notification of group members
            im = createNotificationMessage();
            im.setConfirmationRequired(false);
            im.setThread(g.getName());
            im.setSubject(LANG_MACHINE, "kicked");
            im.setSubject(lang,
                m_BundleMessages.get(
                    lang,
                    "GroupMessagingService.notification.kick.subject",
                    "group",
                    g.getName()
                )
            );
            im.setBody(lang,
                m_BundleMessages.get(
                    lang,
                    "GroupMessagingService.notification.kick.body",
                    "by",
                    caller.getAgentIdentifier().getIdentifier(),
                    "who",
                    aid.getIdentifier()
                )
            );
            //3. Multicast (will also run through the execution service)
            m_Services.getMessagingService().multicast(m_ServicePresenceProxy.getPresence(), im, receivers);
          } catch (Exception ex) {
            Activator.log().error(m_LogMarker, "kickFromGroup()", ex);
          }
        }//run
      });
    }
  }//kickFromGroup

  public void joinGroup(Presence p, String gname)
      throws SecurityException, MessagingException, NoSuchGroupException {

    //2. Existance check
    final GroupImpl g = m_GroupManager.getGroup(gname);
    final AgentIdentifier who = p.getAgentIdentifier();

    if (m_GroupManager.join(gname, p.getAgentIdentifier())) {
      ensureSubscriptionTo(p.getAgentIdentifier());
      //notify
      final Set<AgentIdentifier> receivers = m_GroupManager.getReceivers(gname, who);
      m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_ServiceAgentProxy.getAuthenticPeer(), new Runnable() {
        public void run() {
          try {
            EditableInteractiveMessage im = createNotificationMessage();
            Locale lang = g.getLanguage();
            im.setConfirmationRequired(false);
            im.setThread(g.getName());
            im.setSubject(LANG_MACHINE, "join");
            im.setSubject(lang,
                m_BundleMessages.get(
                    lang,
                    "GroupMessagingService.notification.join.subject"
                )
            );
            im.setBody(lang,
                m_BundleMessages.get(
                    lang,
                    "GroupMessagingService.notification.join.body",
                    "who",
                    who.getIdentifier()
                )
            );
            //3. Multicast (will also run through the execution service)
            m_Services.getMessagingService().multicast(m_ServicePresenceProxy.getPresence(), im, receivers);
          } catch (Exception ex) {
            Activator.log().error(m_LogMarker, "joinGroup()", ex);
          }
        }//run
      });

    }

  }//joinGroup

  public void leaveGroup(Presence p, String gname)
      throws SecurityException, MessagingException, NoSuchGroupException {

    //2. Existance check
    final GroupImpl g = m_GroupManager.getGroup(gname);
    final AgentIdentifier who = p.getAgentIdentifier();

    if (m_GroupManager.leave(gname, p.getAgentIdentifier())) {
      //notify
      final Set<AgentIdentifier> receivers = m_GroupManager.getReceivers(gname, null);
      m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_ServiceAgentProxy.getAuthenticPeer(), new Runnable() {
        public void run() {
          try {
            EditableInteractiveMessage im = createNotificationMessage();
            Locale lang = g.getLanguage();
            im.setConfirmationRequired(false);
            im.setThread(g.getName());
            im.setSubject(LANG_MACHINE, "leave");
            im.setSubject(lang,
                m_BundleMessages.get(
                    lang,
                    "GroupMessagingService.notification.leave.subject"
                )
            );
            im.setBody(lang,
                m_BundleMessages.get(
                    lang,
                    "GroupMessagingService.notification.leave.body",
                    "who",
                    who.getIdentifier()
                )
            );
            //3. Multicast (will also run through the execution service)
            m_Services.getMessagingService().multicast(m_ServicePresenceProxy.getPresence(), im, receivers);
          } catch (Exception ex) {
            Activator.log().error(m_LogMarker, "joinGroup()", ex);
          }
        }//run
      });
    }
  }//leaveGroup

  public EditableInteractiveMessage createGroupMessage(Presence p, String gname, String id)
      throws NoSuchGroupException, MessagingException {

    //1. Ensure presence and registration
    m_Services.getMessagingService().ensureRegistration(p);

    //2. Ensure Group Membership
    m_GroupManager.ensurePresentMember(gname, p.getAgentIdentifier());

    EditableInteractiveMessage im = m_Services.getMessagingService().create(
        p,
        InteractiveMessageTypes.GROUPCHAT,
        (id == null) ? m_Services.getUIDGeneratorService(ServiceMediator.NO_WAIT).getUID() : id
    );
    im.setThread(gname);

    m_GroupMessageTracker.put(im.getIdentifier(), im);
    return im;
  }//createGroupMessage

  public void send(Presence p, String gname, InteractiveMessage im)
      throws MessagingException, IllegalStateException {

    AgentIdentifier sender = p.getAgentIdentifier();
    //1. Ensure presence and registration
    m_Services.getMessagingService().ensureRegistration(p);

    //2. Ensure Group Membership
    m_GroupManager.ensurePresentMember(gname, sender);

    //3. Ensure its a message created here
    if (m_GroupMessageTracker.remove(im.getIdentifier()) != null) {
      //3. Multicast (will also run through the execution service)
      m_Services.getMessagingService().multicast(m_ServicePresenceProxy.getPresence(), im,
          m_GroupManager.getReceivers(gname, sender));
    }
  }//send

  public boolean isGroupMessagingAgent(AgentIdentifier aid) {
    return m_ServiceAgentProxy.getAuthenticPeer().getAgentIdentifier().equals(aid);
  }//isGroupMessagingIdentifier

  //*** Event Handlers ***//

  //should run async (eg. ExecutionService)

  protected void handlePresence(final AgentIdentifier aid) {
    m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_ServiceAgentProxy.getAuthenticPeer(), new Runnable() {
      public void run() {
        try {
          Set<String> mems = m_GroupManager.getMemberOf(aid, false);
          for (String gname : mems) {
            //This will load group into mem if not there
            GroupImpl g = m_GroupManager.getGroup(gname);
            if (g.isPermanent()) {
              if (g.m_Members.contains(aid) && !g.m_PresentMembers.contains(aid)) {
                g.m_PresentMembers.add(aid);
                Activator.log().debug(m_LogMarker, "handlePresence()::Added " + aid.getIdentifier() + " to " + gname);
                try {
                  EditableInteractiveMessage im = createNotificationMessage();
                  Locale lang = g.getLanguage();
                  im.setConfirmationRequired(false);
                  im.setThread(g.getName());
                  im.setSubject(LANG_MACHINE, "presence");
                  im.setSubject(lang,
                      m_BundleMessages.get(
                          lang,
                          "GroupMessagingService.notification.presence.subject",
                          "group",
                          g.getName()
                      )
                  );
                  im.setBody(lang,
                      m_BundleMessages.get(
                          lang,
                          "GroupMessagingService.notification.presence.body",
                          "who",
                          aid.getIdentifier()
                      )
                  );
                  //3. Multicast (will also run through the execution service)
                  m_Services.getMessagingService().multicast(m_ServicePresenceProxy.getPresence(), im, m_GroupManager.getReceivers(g.getName(), aid));

                } catch (Exception ex) {
                  Activator.log().error(m_LogMarker, "handlePresence()", ex);
                }
              }
            }
          }//for
          // 2. check for moderator in temporary groups
          for (GroupImpl g : m_GroupManager.getModeratedTemporary(aid)) {
            if (!g.m_PresentMembers.contains(aid)) {
              g.m_PresentMembers.add(aid);
              Activator.log().debug(m_LogMarker, "handlePresence()::Added " + aid.getIdentifier() + " to " + g.getName());
              try {
                EditableInteractiveMessage im = createNotificationMessage();
                Locale lang = g.getLanguage();
                im.setConfirmationRequired(false);
                im.setThread(g.getName());
                im.setSubject(LANG_MACHINE, "presence");
                im.setSubject(lang,
                    m_BundleMessages.get(
                        lang,
                        "GroupMessagingService.notification.presence.subject",
                        "group",
                        g.getName()
                    )
                );
                im.setBody(lang,
                    m_BundleMessages.get(
                        lang,
                        "GroupMessagingService.notification.presence.body",
                        "who",
                        aid.getIdentifier()
                    )
                );
                //3. Multicast (will also run through the execution service)
                m_Services.getMessagingService().multicast(m_ServicePresenceProxy.getPresence(), im, m_GroupManager.getReceivers(g.getName(), aid));

              } catch (Exception ex) {
                Activator.log().error(m_LogMarker, "handlePresence()", ex);
              }

            }
          }
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, "handlePresence()", ex);
        }
      }//run
    }//runnable
    );//execute
  }//handlePresence

  //should run async (eg. ExecutionService)
  protected void handleAbsence(final AgentIdentifier aid) {
    m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_ServiceAgentProxy.getAuthenticPeer(), new Runnable() {
      public void run() {
        synchronized (m_GroupManager.m_ActiveGroups) {
          for (Iterator<Map.Entry<String, GroupImpl>> iterator = m_GroupManager.m_ActiveGroups.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<String, GroupImpl> entry = iterator.next();
            GroupImpl g = entry.getValue();
            if (g.m_PresentMembers.remove(aid)) {
              Activator.log().debug(m_LogMarker, "handleAbsence()::Removed " + aid.getIdentifier() + " from " + g.getName());
              Set<AgentIdentifier> rec = null;
              try {
                rec = m_GroupManager.getReceivers(g.getName(), null);
              } catch (MessagingException mex) {
                Activator.log().error(m_LogMarker, "handleAbsence()", mex);
              }
              if (rec != null && !rec.isEmpty()) {
                try {
                  EditableInteractiveMessage im = createNotificationMessage();
                  Locale lang = g.getLanguage();
                  im.setConfirmationRequired(false);
                  im.setThread(g.getName());
                  im.setSubject(LANG_MACHINE, "absence");
                  im.setSubject(lang,
                      m_BundleMessages.get(
                          lang,
                          "GroupMessagingService.notification.absence.subject",
                          "group",
                          g.getName()
                      )
                  );
                  im.setBody(lang,
                      m_BundleMessages.get(
                          lang,
                          "GroupMessagingService.notification.absence.body",
                          "who",
                          aid.getIdentifier()
                      )
                  );
                  //3. Multicast (will also run through the execution service)
                  m_Services.getMessagingService().multicast(m_ServicePresenceProxy.getPresence(), im, rec);
                } catch (Exception ex) {
                  Activator.log().error(m_LogMarker, "handleAbsence()", ex);
                }
              }
            }
            //this will unload a permanent or remove a temporary group
            if (g.m_PresentMembers.isEmpty()) {
              iterator.remove();
              if (!g.isPermanent()) {
                //temporary groups need complete removal
                m_GroupManager.m_GroupNames.remove(g.getName());
              }
            }
          }
        }
      }//run
    }//runnable
    );//execute
  }//handleAbsence

  public void doMaintenance(Agent caller) throws MaintenanceException, SecurityException {
    Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.start" + "FCK"));

    //check rights
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), Maintainable.DO_MAINTENANCE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(
          m_LogMarker,
          m_BundleMessages.get("error.action", "action", Maintainable.DO_MAINTENANCE.getIdentifier(),
              "caller", caller.getIdentifier()), ex);
      return;
    }
    try {
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.start"));

      //maintain store
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.do.store"));
      m_Store.maintain(m_ServiceAgentProxy);

      //Group cleanup
      m_GroupManager.maintain();

      //clear trackers; this means that actually happening transactions will fail
      //however, it will remove any possibly existing stale transactions.
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.do.trackers"));
      m_CreatePGTracker.clear();
      m_UpdatePGTracker.clear();
      m_GroupMessageTracker.clear();

      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.end"));
    } catch (Exception ex) {
      throw new MaintenanceException(ex.getMessage(), ex);
    }
  }//doMaintenance

  //*** Listeners ***//
  class PresenceServiceListenerImpl
      extends PresenceServiceListenerAdapter {

    public void found(PresenceProxy p) {
      if (p.isAvailable()) {
        handlePresence(p.getAgentIdentifier());
      }
    }//found

    public void receivedPresence(PresenceProxy p) {
      if (p.isAvailable()) {
        handlePresence(p.getAgentIdentifier());
      }
    }//receivedPresence


    public void becamePresent(PresenceProxy p) {
      if (p.isAvailable()) {
        handlePresence(p.getAgentIdentifier());
      }
    }//becamePresent

    public void becameAbsent(PresenceProxy p) {
      handleAbsence(p.getAgentIdentifier());
    }//becameAbsent

    public void statusUpdated(PresenceProxy p) {
      if (p.isAvailable()) {
        handlePresence(p.getAgentIdentifier());
      }
      if (p.isUnavailable()) {
        handleAbsence(p.getAgentIdentifier());
      }
    }//statusUpdated

    public void subscriptionDenied(AgentIdentifier aid, String reason) {
      m_GroupManager.handleNoSubscription(aid);
    }//subscriptionDenied

    public void subscriptionCanceled(AgentIdentifier aid) {
      m_GroupManager.handleNoSubscription(aid);
    }//subscriptionCanceled

    public void requestedSubscription(PresenceProxy p) {
      m_ServicePresenceProxy.getPresenceService().denySubscription(
          m_ServicePresenceProxy.getPresence(),
          p.getAgentIdentifier(),
          "service"
      );
    }//requestedSubscription

  }//inner class PresenceServiceListenerImpl

  private static Action CREATE_PERMANENT_GROUP = new Action("createPermanentGroup");
  private static Action REMOVE_PERMANENT_GROUP = new Action("removePermanentGroup");
  private static Action UPDATE_PERMANENT_GROUP = new Action("updatePermanentGroup");
  private static Action CREATE_TEMPORARY_GROUP = new Action("createTemporaryGroup");
  private static Action LIST_GROUPS = new Action("listGroups");
  private static Action GET_GROUP = new Action("getGroup");
  private static Action MODERATE_GROUP = new Action("moderateGroup");
  private static Action LIST_MEMBERSHIPS = new Action("listGroupMemberships");
  private static Action LIST_INVITATIONS = new Action("listGroupInvitations");
  private static Action EXISTS_GROUP = new Action("existsGroup");

  private static Action[] ACTIONS = {
      CREATE_PERMANENT_GROUP, REMOVE_PERMANENT_GROUP, UPDATE_PERMANENT_GROUP,
      CREATE_TEMPORARY_GROUP, LIST_GROUPS, GET_GROUP, MODERATE_GROUP, EXISTS_GROUP,
      LIST_MEMBERSHIPS, LIST_INVITATIONS, Maintainable.DO_MAINTENANCE
  };

  private static Locale LANG_MACHINE = new Locale("machine");

}//class GroupMessagingServiceImpl
