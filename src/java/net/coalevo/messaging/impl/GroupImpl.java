/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.messaging.model.EditableGroup;
import net.coalevo.messaging.model.Group;

import java.util.*;

/**
 * Provides an implementation of {@link Group}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class GroupImpl
    implements Group {

  protected String m_Name;
  protected Locale m_Language;
  protected String m_InfoFormat;
  protected String m_Info;
  protected Set<String> m_Tags;
  protected AgentIdentifier m_Moderator;

  protected boolean m_Permanent;
  protected boolean m_InviteOnly;
  protected boolean m_InviteModerated;

  protected long m_Created;

  protected Set<AgentIdentifier> m_Members;
  protected Set<AgentIdentifier> m_PresentMembers;
  protected Set<AgentIdentifier> m_Invited;

  public GroupImpl(String name, boolean permanent) {
    m_Name = name;
    m_Permanent = permanent;
    m_Created = System.currentTimeMillis();
    if (permanent) {
      m_Members = Collections.synchronizedSet(new HashSet<AgentIdentifier>());
    }
    m_PresentMembers = Collections.synchronizedSet(new HashSet<AgentIdentifier>());
    m_Invited = Collections.synchronizedSet(new HashSet<AgentIdentifier>());

  }//GroupImpl

  public GroupImpl(String name, Locale lang, String informat, String info,
                   Set<String> tags, AgentIdentifier moderator,
                   boolean permanent, boolean inviteonly,
                   boolean invitemoderated, long created) {
    m_Name = name;
    m_Language = lang;
    m_InfoFormat = informat;
    m_Info = info;
    m_Tags = tags;
    m_Moderator = moderator;
    m_Permanent = permanent;
    m_InviteOnly = inviteonly;
    m_InviteModerated = invitemoderated;
    m_Created = created;
    if (permanent) {
      m_Members = Collections.synchronizedSet(new HashSet<AgentIdentifier>());
    }
    m_PresentMembers = Collections.synchronizedSet(new HashSet<AgentIdentifier>());
    m_Invited = Collections.synchronizedSet(new HashSet<AgentIdentifier>());
  }//GroupImpl

  public String getName() {
    return m_Name;
  }//getName

  public Locale getLanguage() {
    return m_Language;
  }//getLanguage

  public String getInfoFormat() {
    return m_InfoFormat;
  }//getInfoFormat

  public void setInfoFormat(String format) {
    m_InfoFormat = format;
  }//setInfoFormat

  public String getInfo() {
    return m_Info;
  }//getInfo

  public void setInfo(String info) {
    synchronized (m_Info) {
      m_Info = info;
    }
  }//setInfo

  public Set<String> getTags() {
    return Collections.unmodifiableSet(m_Tags);
  }//getTags

  public Set<AgentIdentifier> getPresentMembers() {
    return Collections.unmodifiableSet(m_PresentMembers);
  }//getPresentMembers

  public Set<AgentIdentifier> getMembers() {
    if (m_Permanent) {
      return Collections.unmodifiableSet(m_Members);
    } else {
      return getPresentMembers();
    }
  }//getMembers

  public Set<AgentIdentifier> getInvited() {
    return Collections.unmodifiableSet(m_Invited);
  }//getInvited

  public long getCreated() {
    return m_Created;
  }//created

  public AgentIdentifier getModerator() {
    return m_Moderator;
  }//getModerator

  public boolean isPermanent() {
    return m_Permanent;
  }//isPermanent

  public boolean isInviteOnly() {
    return m_InviteOnly;
  }//isInviteOnly

  public boolean isInviteModerated() {
    return m_InviteModerated;
  }//isInviteModerated  

  public void becameAvailable(AgentIdentifier aid) {
    if (m_Permanent) {
      if (!m_PresentMembers.contains(aid) && m_Members.contains(aid)) {
        m_PresentMembers.add(aid);
      }
    }
  }//becameAvailable

  public void becameUnavailable(AgentIdentifier aid) {
    if (m_Permanent) {
      m_PresentMembers.remove(aid);
    }
  }//becameUnavailable

  public boolean isInvited(AgentIdentifier aid) {
    return m_Invited.contains(aid);
  }//isInvited

  public boolean isMember(AgentIdentifier aid) {
    if (m_Permanent) {
      return m_Members.contains(aid);
    } else {
      return m_PresentMembers.contains(aid);
    }
  }//isMember

  public EditableGroup toEditableGroup() {
    if (isPermanent()) {
      //TODO: probably lock other editing ?
      return new EditableGroupImpl();
    } else {
      throw new UnsupportedOperationException();
    }
  }//toEditableGroup

  public String toString() {
    StringBuilder sbuf = new StringBuilder(super.toString());
    sbuf.append("[");
    sbuf.append(m_Name);
    sbuf.append(',');
    sbuf.append(m_Language.getLanguage());
    sbuf.append(',');
    sbuf.append(m_InfoFormat);
    sbuf.append(',');
    sbuf.append(m_Info);
    sbuf.append(',');
    sbuf.append(m_Tags.toString());
    sbuf.append(',');
    sbuf.append(m_Moderator.getIdentifier());
    sbuf.append(',');
    sbuf.append((m_Permanent) ? "permanent" : "temporary");
    sbuf.append(',');
    sbuf.append((m_InviteOnly) ? "invite only" : "public");
    sbuf.append(',');
    sbuf.append((m_InviteModerated) ? "invite moderated" : "invite members");
    sbuf.append(',');
    sbuf.append(new Date(m_Created).toString());
    if (m_Permanent) {
      sbuf.append(",M:");
      sbuf.append(m_Members.toString());
    }
    sbuf.append(",PM:");
    sbuf.append(m_PresentMembers.toString());
    sbuf.append(",I:");
    sbuf.append(m_Invited);
    return sbuf.toString();
  }//toString

  class EditableGroupImpl
      implements EditableGroup {

    public void setLanguage(Locale l) {
      m_Language = l;
    }//setLanguage

    public void setInfoFormat(String format) {
      m_InfoFormat = format;
    }//setInfoFormat

    public void setModerator(AgentIdentifier aid) {
      m_Moderator = aid;
      m_Members.add(aid);
    }//setModerator

    public void setTags(Set<String> tags) {
      m_Tags = tags;
    }//setTags

    public void addTag(String tag) {
      if (m_Tags == null) {
        m_Tags = new HashSet<String>();
      }
      m_Tags.add(tag);
    }//addTag

    public void removeTag(String tag) {
      m_Tags.remove(tag);
    }//removeTag

    public void setInviteOnly(boolean b) {
      m_InviteOnly = b;
    }//setInviteOnly

    public void setInviteModerated(boolean b) {
      m_InviteModerated = b;
    }//setInviteModerated

    public String getName() {
      return m_Name;
    }//getName

    public Locale getLanguage() {
      return m_Language;
    }//getLanguage

    public String getInfo() {
      return m_Info;
    }//getInfo

    public void setInfo(String info) {
      m_Info = info;
    }//setInfo

    public String getInfoFormat() {
      return m_InfoFormat;
    }//getInfoFormat

    public AgentIdentifier getModerator() {
      return m_Moderator;
    }//getModerator

    public Set<String> getTags() {
      return m_Tags;
    }//getTags

    public Set<AgentIdentifier> getPresentMembers() {
      return GroupImpl.this.getPresentMembers();
    }//getPresentMembers

    public Set<AgentIdentifier> getMembers() {
      return GroupImpl.this.getMembers();
    }//getMembers

    public Set<AgentIdentifier> getInvited() {
      return GroupImpl.this.getInvited();
    }//getInvited

    public long getCreated() {
      return m_Created;
    }//getCreated

    public boolean isInviteOnly() {
      return m_InviteOnly;
    }//isInviteOnly

    public boolean isInviteModerated() {
      return m_InviteModerated;
    }//isInviteModerated

    public boolean isPermanent() {
      return m_Permanent;
    }//isPermanent

    public GroupImpl toGroup() {
      return GroupImpl.this;
    }//toGroup

  }//inner class EditableGroupImpl

}//class GroupImpl
