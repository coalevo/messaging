/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.impl;

import net.coalevo.foundation.model.*;
import net.coalevo.foundation.service.UUIDGeneratorService;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.messaging.model.*;
import net.coalevo.messaging.service.MessagingConfiguration;
import net.coalevo.messaging.service.MessagingService;
import net.coalevo.presence.model.Presence;
import net.coalevo.presence.model.PresenceConstants;
import net.coalevo.presence.model.PresenceRegistrationListener;
import net.coalevo.presence.service.PresenceService;
import net.coalevo.security.model.NoSuchActionException;
import net.coalevo.security.model.PolicyProxy;
import net.coalevo.security.model.ServiceAgentProxy;
import org.apache.commons.codec.binary.Base64;
import org.osgi.framework.BundleContext;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.ResultSet;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Implements {@link MessagingService}.
 * <p/>
 * Todo: Enforce queue limit. Remove older messages? What would be the policy?
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MessagingServiceImpl
    extends BaseService
    implements MessagingService, Maintainable, ConfigurationUpdateHandler {

  private Marker m_LogMarker = MarkerFactory.getMarker(MessagingServiceImpl.class.getName());

  private ServiceMediator m_Services;
  private Messages m_BundleMessages;

  // Security context
  private ServiceAgentProxy m_ServiceAgentProxy;
  private PolicyProxy m_PolicyProxy;

  private PresenceRegistrationListener m_PresenceRegistrationListener;

  private MessagingStore m_Store;
  private Map<Presence, MessagingServiceListener> m_Registered;
  private AtomicBoolean m_Active;
  private Set<EventInfo> m_StartedMessages;
  private Set<EventInfo> m_Confirmables;
  private FixedSizeSet m_StartStopBuffer;

  private Map<AgentIdentifier, MessagingServiceListener> m_Listeners;
  private EventManager m_EventManager;
  //private Map m_RRProviders;

  private long m_ConfirmationExpiry = 900000;
  private long m_StartStopExpiry = 900000;
  private int m_MaxQueuedOfflineMsg = 50;

  public MessagingServiceImpl(MessagingStore store) {
    super(MessagingService.class.getName(), ACTIONS);
    m_Store = store;
    m_Registered = new ConcurrentHashMap<Presence, MessagingServiceListener>();
    m_StartStopBuffer = new FixedSizeSet<EventInfo>(100,
        new FixedSizeSet.EntryExpungeHandler<EventInfo>() {
          public void expunged(EventInfo m) {
            if (m_Active.get() && m_EventManager != null) {
              m_EventManager.notifyStopped(m);
            }
          }//expunged
        });
    m_StartedMessages = Collections.synchronizedSet(m_StartStopBuffer);

    m_Listeners = new ConcurrentHashMap<AgentIdentifier, MessagingServiceListener>();
    m_Confirmables = Collections.synchronizedSet(new FixedSizeSet<EventInfo>(100));
    m_Active = new AtomicBoolean(false);
    m_PresenceRegistrationListener = new PresenceRegistrationListenerImpl();
    //m_RRProviders = new ConcurrentHashMap();
  }//constructor

  public boolean activate(BundleContext bc) {
    if (m_Active.getAndSet(true)) {
      return false;
    }
    m_Services = Activator.getServices();
    m_BundleMessages = Activator.getBundleMessages();

    //1. Authenticate ourself
    m_ServiceAgentProxy = new ServiceAgentProxy(this, Activator.log());
    m_ServiceAgentProxy.activate(bc);

    //2. Policy
    m_PolicyProxy = new PolicyProxy(
        getIdentifier(),
        "COALEVO-INF/security/MessagingService-policy.xml",
        m_ServiceAgentProxy,
        Activator.log()
    );
    m_PolicyProxy.activate(bc);

    // Presence
    m_Services.getPresenceService(ServiceMediator.WAIT_UNLIMITED).addPresenceRegistrationListener(
        m_ServiceAgentProxy.getAuthenticPeer(), m_PresenceRegistrationListener);

    // Create and configure EventManager
    m_EventManager = new EventManager();
    m_Services.getConfigMediator().addUpdateHandler(m_EventManager);
    m_EventManager.update(m_Services.getConfigMediator().getConfiguration());

    // Ensure service configuration
    m_Services.getConfigMediator().addUpdateHandler(this);
    update(m_Services.getConfigMediator().getConfiguration());

    return true;
  }//activate

  public boolean deactivate() {
    if (m_Active.getAndSet(false)) {
      if (m_Registered != null) {
        m_Registered.clear();
        m_Registered = null;
      }
      if (m_Listeners != null) {
        m_Listeners.clear();
        m_Listeners = null;
      }
      if (m_StartedMessages != null) {
        m_StartedMessages.clear();
        m_StartedMessages = null;
      }
      try {
        PresenceService ps = m_Services.getPresenceService(ServiceMediator.NO_WAIT);
        if (ps != null) {
          ps.removePresenceRegistrationListener(
              m_ServiceAgentProxy.getAuthenticPeer(), m_PresenceRegistrationListener
          );
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "deactivate()", ex);
      }

      if (m_PolicyProxy != null) {
        m_PolicyProxy.deactivate();
        m_PolicyProxy = null;
      }
      if (m_ServiceAgentProxy != null) {
        m_ServiceAgentProxy.deactivate();
        m_ServiceAgentProxy = null;
      }

      m_BundleMessages = null;
      return true;
    }
    return false;
  }//deactivate

  public boolean register(Presence p, MessagingServiceListener msl) {
    PresenceService ps = m_Services.getPresenceService(5000);
    try {
      if (ps.isRegistered(m_ServiceAgentProxy.getAuthenticPeer(), p)) {
        m_Registered.put(p, msl);
        m_Listeners.put(p.getAgentIdentifier(), msl);
        flushOfflineMessages(p.getAgentIdentifier());
        Agent a = p.getAgent();
        if (a instanceof UserAgent) {
          Session s = ((UserAgent) a).getSession();
          if (s != null && s.isValid()) {
            s.addSessionListener(
                new SessionListener() {
                  public void invalidated(Session s) {
                    Presence p = (Presence) s.getAttribute(PresenceConstants.PRESENCE_SESSION_ATTRIBUTE);
                    if (MessagingServiceImpl.this.m_Registered.containsKey(p)) {
                      MessagingServiceImpl.this.unregister(p);//unregister
                    }
                  }//invalidated
                }//anonymous SessionListener
            );
          }
        }
        return true;
      }
    } catch (SecurityException ex) {
      Activator.log().error(m_LogMarker, "register()", ex);
    }
    return false;
  }//register

  public void unregister(Presence p) {
    if (m_Registered != null) {
      m_Registered.remove(p);
    }
    if (m_Listeners != null) {
      m_Listeners.remove(p.getAgentIdentifier());
    }
  }//unregister

  protected void ensureRegistration(Presence p)
      throws MessagingException {
    if (isRegistered(p)) {
      return;
    } else {
      //TODO: i18n message
      throw new MessagingException();
    }
  }//ensureRegistration

  public void send(Presence p, Message m)
      throws MessagingException {
    ensureRegistration(p);
    if (m instanceof InteractiveMessage) {
      sendInteractiveMessage((InteractiveMessage) m);
      return;
    }
    //request & response
    //TODO: SPAM control?
    m_EventManager.send(m);
  }//send

  private void sendInteractiveMessage(InteractiveMessage m) {
    //TODO: Policy checks?
    if (isMessageable(m.getTo())) {
      if (m_StartedMessages.remove(m)) {
        m_EventManager.notifyStopped(m);
      }
      if (m.isConfirmationRequired()) {
        m_Confirmables.add(m.getEventInfo());
      }
      m_EventManager.send(m);
    }
  }//sendInteractiveMessage

  public void unicast(Presence p, InteractiveMessage m)
      throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(p.getAgent()), UNICAST);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "unicast()", ex);
    }
    m_EventManager.send(m);
  }//unicast

  public void broadcast(Presence p, InteractiveMessage m) throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(p.getAgent()), BROADCAST);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "broadcast()", ex);
    }
    if (m instanceof InteractiveMessageImpl) {
      ((InteractiveMessageImpl) m).setBroadcast(true);
    }
    m_EventManager.broadcast((InteractiveMessageImpl) m);
  }//broadcast

  public void multicast(Presence p, InteractiveMessage m, Set<AgentIdentifier> receivers)
      throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(p.getAgent()), MULTICAST);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "multicast()", ex);
    }

    m_EventManager.multicast((InteractiveMessageImpl) m, receivers);
  }//multicast

  public long confirm(Presence p, EventInfo info)
      throws MessagingException {
    ensureRegistration(p);
    if (m_Confirmables.remove(info)) {
      return m_EventManager.confirm(info);
    }
    return -1;
  }//confirm

  //done
  public void started(Presence p, EventInfo info)
      throws MessagingException {
    ensureRegistration(p);
    if (m_StartedMessages.add(info)) {
      m_EventManager.notifyStarted(info);
    }
  }//started

  //done
  public void stopped(Presence p, EventInfo info)
      throws MessagingException {
    ensureRegistration(p);
    if (m_StartedMessages.remove(info)) {
      m_EventManager.notifyStopped(info);
    }
  }//stopped

  public boolean store(Presence p, EditableInteractiveMessage im)
      throws MessagingException {
    AgentIdentifier to = im.getTo();
    //1. Standard Checks
    if (!isRegistered(p)
        || isMessageable(to)
        || !p.getSubscriptionsTo().contains(to)) {
      return false;
    }

    //2. Ensure settings for offline ims
    im.setConfirmationRequired(false); //cannot be confirmed
    im.setType(InteractiveMessageTypes.OFFLINE);  // must be offline

    //3. Serialize into byte[] max 32k, so beware.
    ByteArrayOutputStream bout = new ByteArrayOutputStream();
    try {
      ObjectOutputStream out = new ObjectOutputStream(bout);
      out.writeObject(im);
    } catch (Exception ex) {
      throw new MessagingException("store()", ex);
    }
    String msgdata = new String(Base64.encodeBase64(bout.toByteArray()));

    //4. Store in database
    HashMap<String, Object> params = new HashMap<String, Object>();
    params.put("agent_id", to.getIdentifier());
    params.put("msg_id", im.getIdentifier());
    params.put("stored", System.currentTimeMillis());
    params.put("msg", msgdata);
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("addOfflineMessage", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "store()::", ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    return true;
  }//store

  public void retrieve(Presence p)
      throws MessagingException {
    //1. Standard Checks
    ensureRegistration(p);
    flushOfflineMessages(p.getAgentIdentifier());
  }//receive

  public EditableInteractiveMessage create(Presence p, InteractiveMessageType type, String uid)
      throws MessagingException {
    ensureRegistration(p);
    UUIDGeneratorService uuidgen = m_Services.getUIDGeneratorService(ServiceMediator.NO_WAIT);
    if(uuidgen == null && (uid == null || uid.length() == 0)) {
      throw new MessagingException();
    }
    InteractiveMessageImpl m = new InteractiveMessageImpl(
        (uid == null) ? uuidgen.getUID() : uid
    );
    m.setType(type);
    m.setFrom(p.getAgentIdentifier());
    return m;
  }//create

  private boolean isMessageable(AgentIdentifier aid) {
    PresenceService ps = m_Services.getPresenceService(ServiceMediator.NO_WAIT);
    return (ps != null
        && ps.isAvailable(m_ServiceAgentProxy.getAuthenticPeer(), aid));
  }//isMessageable

  private boolean isRegistered(Presence p) {
    return m_Registered.containsKey(p);
  }//isRegistered

  private void flushOfflineMessages(final AgentIdentifier aid) {
    m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(
        m_ServiceAgentProxy.getAuthenticPeer(), new Runnable() {

          public void run() {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("agent_id", aid.getIdentifier());
            LibraryConnection lc = null;
            ResultSet rs = null;
            try {
              lc = m_Store.leaseConnection();
              rs = lc.executeQuery("getOfflineMessages", params);
              while (rs.next()) {
                ObjectInputStream oin = new ObjectInputStream(
                    new ByteArrayInputStream(
                        Base64.decodeBase64(rs.getString(3).getBytes())
                    )
                );
                InteractiveMessageImpl msg = (InteractiveMessageImpl) oin.readObject();
                msg.setThread(Long.toString(rs.getLong(2)));
                m_EventManager.send(msg);
                params.put("msg_id", rs.getString(1));
                lc.executeUpdate("removeOfflineMessage", params);
              }
            } catch (Exception ex) {
              Activator.log().error(m_LogMarker, "flushOfflineMessages()::", ex);
            } finally {
              SqlUtils.close(rs);
              m_Store.releaseConnection(lc);
            }
          }//run
        });
  }//flushOfflineMessages

  public void doMaintenance(Agent caller) throws MaintenanceException, SecurityException {
    //check rights
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), Maintainable.DO_MAINTENANCE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", Maintainable.DO_MAINTENANCE.getIdentifier(), "caller", caller.getIdentifier()), ex);
      return;
    }
    try {
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.start"));

      //Store maintenance is called by GroupMessagingService !
      //cleanup stale refs
      long now = System.currentTimeMillis();
      for (Iterator<EventInfo> iter = m_Confirmables.iterator(); iter.hasNext();) {
        EventInfo ei = iter.next();
        long stale = now - ei.getCreated();
        if (stale > m_ConfirmationExpiry) {
          iter.remove();
        }
      }
      for (Iterator<EventInfo> iter = m_StartedMessages.iterator(); iter.hasNext();) {
        EventInfo ei = iter.next();
        long stale = now - ei.getCreated();
        if (stale > m_StartStopExpiry) {
          iter.remove();
        }
      }

      //Some debug logging
      Activator.log().debug(m_LogMarker, "m_Registered.size()=" + m_Registered.size());
      Activator.log().debug(m_LogMarker, "m_Listeners.size()=" + m_Listeners.size());

      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.end"));
    } catch (Exception ex) {
      throw new MaintenanceException(ex.getMessage(), ex);
    }
  }//doMaintenance

  public void update(MetaTypeDictionary mtd) {
    int ssbuf = 100;
    //1. Configure from persistent configuration
    try {
      m_ConfirmationExpiry = mtd.getLong(MessagingConfiguration.CONFIRMATION_EXPIRY_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingServiceImpl.configexception",
              "attribute",
              MessagingConfiguration.CONFIRMATION_EXPIRY_KEY),
          ex
      );
    }
    try {
      m_StartStopExpiry = mtd.getLong(MessagingConfiguration.STARTSTOP_EXPIRY_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingServiceImpl.configexception",
              "attribute",
              MessagingConfiguration.STARTSTOP_EXPIRY_KEY),
          ex
      );
    }
    try {
      ssbuf = mtd.getInteger(MessagingConfiguration.STARTSTOP_BUFFER_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingServiceImpl.configexception", "attribute", MessagingConfiguration.STARTSTOP_BUFFER_KEY),
          ex
      );
    }
    try {
      m_MaxQueuedOfflineMsg = mtd.getInteger(MessagingConfiguration.OFFLINEMSG_MAXQUEUED_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("MessagingServiceImpl.configexception", "attribute", MessagingConfiguration.OFFLINEMSG_MAXQUEUED_KEY),
          ex
      );
    }
    //update buffer
    m_StartStopBuffer.setMaxSize(ssbuf);
  }//update


  class EventManager extends MessagingEventManager {

    public void broadcast(InteractiveMessageImpl m) {
      Broadcaster br = null;
      try {
        br = (Broadcaster) m_BroadcasterPool.borrowObject();
        br.setMessage(m);
        m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_ServiceAgentProxy.getAuthenticPeer(), br);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "broadcast()", ex);
      }
    }//broadcast

    public void multicast(InteractiveMessageImpl m, Set<AgentIdentifier> receivers) {
      Broadcaster br = null;
      try {
        br = (Broadcaster) m_BroadcasterPool.borrowObject();
        br.setMessage(m);
        br.setReceivers(receivers);
        m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_ServiceAgentProxy.getAuthenticPeer(), br);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "multicast()", ex);
      }
    }//multicast

    public long confirm(EventInfo info) {
      Confirmer cf = null;
      try {
        cf = (Confirmer) m_ConfirmerPool.borrowObject();
        long ts = cf.set(info);
        m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_ServiceAgentProxy.getAuthenticPeer(), cf);
        return ts;
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "confirm()", ex);
      }
      return -1;
    }//confirm

    public void send(Message m) {
      MessageDelivery md = null;

      try {
        md = (MessageDelivery) m_MessageDeliveryPool.borrowObject();
        md.setMessage(m);
        m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_ServiceAgentProxy.getAuthenticPeer(), md);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "send()", ex);
      }
    }//send

    public void notifyStarted(EventInfo info) {
      EventNotify notify = null;
      try {
        notify = (EventNotify) m_EventNotifyPool.borrowObject();
        //need to set from and to and the identifier
        notify.setStart(info.getFrom(), info.getTo(), info.getIdentifier());
        m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_ServiceAgentProxy.getAuthenticPeer(), notify);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "notifyEvent()", ex);
      }
    }//notifyStarted

    public void notifyStopped(EventInfo info) {
      EventNotify notify = null;
      try {
        notify = (EventNotify) m_EventNotifyPool.borrowObject();
        //need to set from and to and the identifier
        notify.setStop(info.getFrom(), info.getTo(), info.getIdentifier());
        m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_ServiceAgentProxy.getAuthenticPeer(), notify);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "notifyEvent()", ex);
      }
    }//notifyStopped

    public MessagingServiceListener getMessagingServiceListener(AgentIdentifier aid) {
      Object msl = m_Listeners.get(aid);
      return (msl == null) ? null : (MessagingServiceListener) msl;
    }//getMessagingServiceListener

    public Iterator getRegistered() {
      return m_Registered.entrySet().iterator();
    }//getRegistered

    public boolean isPresenceAvailable(AgentIdentifier aid) {
      return m_Listeners.containsKey(aid);
    }//isPresenceAvailable

    protected void addConfirmable(EventInfo ei) {
      m_Confirmables.add(ei);
    }//addConfirmable

  }//EventManager

  class PresenceRegistrationListenerImpl
      implements PresenceRegistrationListener {

    public void unregistered(Presence p) {
      MessagingServiceImpl.this.unregister(p);
    }//unregistered

  }//class PresenceRegistrationListenerImpl


  private static Action BROADCAST = new Action("broadcast");
  private static Action MULTICAST = new Action("multicast");
  private static Action UNICAST = new Action("unicast");

  private static Action SET_MESSAGE_FACTORY = new Action("setInteractiveMessageFactory");

  private static Action[] ACTIONS =
      {BROADCAST, MULTICAST, UNICAST, SET_MESSAGE_FACTORY, Maintainable.DO_MAINTENANCE};


}//class MessagingServiceImpl
