/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.service;

/**
 * Defines a tag interface for the registration of the
 * unified bundle configuration.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 * @see net.coalevo.foundation.util.BundleConfiguration
 */
public interface MessagingConfiguration {

  public static final String DATA_SOURCE_KEY = "datasource";
  public static final String CONNECTION_POOLSIZE_KEY = "connections.poolsize";
  public static final String AGENTIDENTIFIERS_CACHESIZE_KEY = "cache.agentidentifiers";
  
  public static final String MSGDELIVERYPOOL_MAXACTIVE_KEY = "msgdelivery.pool.maxactive";
  public static final String MSGDELIVERYPOOL_MAXIDLE_KEY = "msgdelivery.pool.maxidle";
  public static final String MSGDELIVERYPOOL_MINIDLE_KEY = "msgdelivery.pool.minidle";
  public static final String MSGDELIVERYPOOL_MAXWAIT_KEY = "msgdelivery.pool.maxwait";
  public static final String MSGDELIVERYPOOL_EXHAUSTACTION_KEY = "msgdelivery.pool.exhaustaction";

  public static final String EVENTNOTIFYPOOL_MAXACTIVE_KEY = "eventnotify.pool.maxactive";
  public static final String EVENTNOTIFYPOOL_MAXIDLE_KEY = "eventnotify.pool.maxidle";
  public static final String EVENTNOTIFYPOOL_MINIDLE_KEY = "eventnotify.pool.minidle";
  public static final String EVENTNOTIFYPOOL_MAXWAIT_KEY = "eventnotify.pool.maxwait";
  public static final String EVENTNOTIFYPOOL_EXHAUSTACTION_KEY = "eventnotify.pool.exhaustaction";


  public static final String BROADCASTPOOL_MAXACTIVE_KEY = "eventnotify.pool.maxactive";
  public static final String BROADCASTPOOL_MAXIDLE_KEY = "eventnotify.pool.maxidle";
  public static final String BROADCASTPOOL_MINIDLE_KEY = "eventnotify.pool.minidle";
  public static final String BROADCASTPOOL_MAXWAIT_KEY = "eventnotify.pool.maxwait";
  public static final String BROADCASTPOOL_EXHAUSTACTION_KEY = "eventnotify.pool.exhaustaction";

  public static final String CONFIRMATIONPOOL_MAXACTIVE_KEY = "confirmation.pool.maxactive";
  public static final String CONFIRMATIONPOOL_MAXIDLE_KEY = "confirmation.pool.maxidle";
  public static final String CONFIRMATIONPOOL_MINIDLE_KEY = "confirmation.pool.minidle";
  public static final String CONFIRMATIONPOOL_MAXWAIT_KEY = "confirmation.pool.maxwait";
  public static final String CONFIRMATIONPOOL_EXHAUSTACTION_KEY = "confirmation.pool.exhaustaction";

  public static final String POOLEXHAUST_GROW = "grow";
  public static final String POOLEXHAUST_FAIL = "fail";
  public static final String POOLEXHAUST_BLOCK = "block";

  public static final String CONFIRMATION_EXPIRY_KEY = "confirmation.expiry";
  public static final String STARTSTOP_EXPIRY_KEY = "startstop.expiry";
  public static final String STARTSTOP_BUFFER_KEY = "startstop.buffer";
  public static final String OFFLINEMSG_EXPIRY_KEY = "offlinemsg.expiry";
  public static final String OFFLINEMSG_MAXQUEUED_KEY = "offlinemsg.maxqueued";
  public static final String INVITATION_EXPIRY_KEY = "invitation.expiry";

}//interface MessagingConfiguration
