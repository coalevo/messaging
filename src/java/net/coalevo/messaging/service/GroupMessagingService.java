/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.service;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Service;
import net.coalevo.messaging.model.*;
import net.coalevo.presence.model.Presence;

import java.util.Locale;
import java.util.Set;

/**
 * Defines the interface for a group messaging service.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface GroupMessagingService
    extends Service {

  /**
   * Begins to create a new permanent {@link Group}.
   *
   * @param caller the requesting {@link Agent}.
   * @param name   the group name.
   * @return an {@link EditableGroup} instance.
   * @throws SecurityException  if the calling {@link Agent} is not authentic or
   *                            not authorized.
   * @throws MessagingException if a group with the same name already exists.
   * @see #commitPermanentGroup(Agent,EditableGroup)
   */
  public EditableGroup beginCreatePermanentGroup(Agent caller, String name)
      throws SecurityException, MessagingException;

  /**
   * Begins the update of an existing permanent {@link Group}.
   *
   * @param caller the requesting {@link Agent}.
   * @param name   the group name.
   * @return an {@link EditableGroup} instance.
   * @throws SecurityException    if the calling {@link Agent} is not authentic or
   *                              not authorized.
   * @throws MessagingException   if an error occurs while executing this action.
   * @throws NoSuchGroupException if a group with the given name does not exist
   *                              is not permanent.
   * @see #commitPermanentGroup(Agent,EditableGroup)
   */
  public EditableGroup beginUpdatePermanentGroup(Agent caller, String name)
      throws SecurityException, MessagingException, NoSuchGroupException;

  /**
   * Commits a new or updated permanent group.
   * <p/>
   * This method should be preceded by {@link #beginUpdatePermanentGroup(Agent,String)}
   * to begin an update or {@link #beginCreatePermanentGroup(Agent,String)} to begin
   * a create (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param eg     an {@link EditableGroup}.
   * @throws SecurityException     if the calling {@link Agent} is not authentic or
   *                               not authorized.
   * @throws IllegalStateException if the transaction was not started either by
   *                               {@link #beginUpdatePermanentGroup(Agent,String)} or by
   *                               {@link #beginCreatePermanentGroup(Agent,String)}.
   * @throws MessagingException    if an error occurs while executing this action.
   * @see #beginUpdatePermanentGroup(Agent,String)
   * @see #beginCreatePermanentGroup(Agent,String)
   */
  public void commitPermanentGroup(Agent caller, EditableGroup eg)
      throws SecurityException, MessagingException;

  /**
   * Cancels a permanent group related transaction.
   * <p/>
   * This method should be preceded by {@link #beginUpdatePermanentGroup(Agent,String)}
   * to begin an update or {@link #beginCreatePermanentGroup(Agent,String)} to begin
   * a create (transaction style).
   * @param caller  the requesting {@link Agent}.
   * @param eg  an {@link EditableGroup}.
   */
  public void cancelPermanentGroupTransaction(Agent caller, EditableGroup eg);
  
  /**
   * Removes a permanent group.
   *
   * @param caller the requesting {@link Agent}.
   * @param gname  the group's name
   * @throws SecurityException    if the calling {@link Agent} is not authentic or
   *                              not authorized.
   * @throws MessagingException   if an error occurs while executing this action.
   * @throws NoSuchGroupException if a group with the given name does not exist
   *                              is not permanent.
   */
  public void removePermanentGroup(Agent caller, String gname)
      throws SecurityException, MessagingException, NoSuchGroupException;

  /**
   * Creates a new temporary group.
   *
   * @param caller     the requesting {@link Agent}.
   * @param name       the name of the new temporary group.
   * @param l          the locale that defines the groups language.
   * @param info       the info about the group.
   * @param infoformat the format the info is written in.
   * @param tags       a comma separated list of tags (String).
   * @param inviteonly whether or not the group is invite only.
   * @param invitemod  whether or not the group is invite moderated.
   * @return a newly created temporary {@link Group} instance.
   * @throws SecurityException  if the calling {@link Agent} is not authentic or
   *                            not authorized.
   * @throws MessagingException if a group with the given name already exists.
   */
  public Group createTemporaryGroup(Agent caller, String name, Locale l, String infoformat, String info, String tags,
                                    boolean inviteonly, boolean invitemod)
      throws SecurityException, MessagingException;

  /**
   * Tests if a group with a given name exists.
   * @param caller the requesting {@link Agent}.
   * @param gname  the group name to be tested.
   * @return true if exists, false otherwise.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                            not authorized.
   */
  public boolean existsGroup(Agent caller, String gname)
      throws SecurityException;

  /**
   * Lists the groups managed by this service.
   *
   * @param caller the requesting {@link Agent}.
   * @param permanent true if list permanent, false if list temporary.
   * @return an unmodifiable set of group names.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public Set<String> listGroups(Agent caller, boolean permanent)
      throws SecurityException;

  /**
   * Lists the groups the requesting agent is invited to.
   *
   * @param caller the requesting {@link Agent}.
   * @return a set of group names that the caller is invited to.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   * @throws MessagingException if an error occurs while executing this action.
   */
  public Set<String> listGroupInvitations(Agent caller)
      throws SecurityException, MessagingException;

  /**
   * Lists the group the specified agent is member of.
   * @param caller the requesting {@link Agent}.
   * @param aid the agent per {@link AgentIdentifier}.
   * @return a set of group names that the specified agent is member of.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   * @throws MessagingException if an error occurs while executing this action.
   */
   public Set<String> listGroupMemberships(Agent caller, AgentIdentifier aid)
      throws SecurityException, MessagingException;

  /**
   * Lists active groups that are accessible to the public.
   *
   * @param caller the requesting {@link Agent}.
   * @return a set of group names that are open to the public.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   * @throws MessagingException if an error occurs while executing this action.
   */
  public Set<String> listPublicGroups(Agent caller)
      throws SecurityException, MessagingException;

  /**
   * Lists the groups the specified agent is invited to.
   *
   * @param caller the requesting {@link Agent}.
   * @param aid the agent per {@link AgentIdentifier}.
   * @return a set of group names that are open to the public.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   * @throws MessagingException if an error occurs while executing this action.
   */
  public Set<String> listGroupInvitations(Agent caller, AgentIdentifier aid)
      throws SecurityException, MessagingException;

  /**
   * Lists all groups a specified agent may join.
   * <p/>
   * This will also list public active groups.
   *
   * @param p the requesting {@link Presence}.
   * @return a set of group names
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   * @throws MessagingException if an error occurs while executing this action.
   */
  public Set<String> listJoinableGroups(Presence p)
      throws SecurityException, MessagingException;

  /**
   * Returns the specified {@link Group}.
   *
   * @param caller the requesting {@link Agent}.
   * @param gname  the group name.
   * @return a {@link Group} instance.
   * @throws SecurityException  if the calling {@link Agent} is not authentic or
   *                            not authorized.
   * @throws MessagingException if an error occurs while executing this action.
   */
  public Group getGroup(Agent caller, String gname)
      throws SecurityException, MessagingException;

  /**
   * Invites a specified agent to a specified group.
   * </p>
   * The implementation ignores duplicate invitations or invitations of
   * agents that are already group members.
   * <p/>
   * If this group is permanent, the invitation will be persisted.
   *
   * @param caller the requesting {@link Agent}.
   * @param gname  the group name.
   * @param aid    the {@link AgentIdentifier} specifying the agent to be invited.
   * @throws SecurityException  if the calling {@link Agent} is not authentic or
   *                            not authorized.
   * @throws MessagingException if an error occurs while executing this action.
   */
  public void inviteToGroup(Agent caller, String gname, AgentIdentifier aid)
      throws SecurityException, MessagingException;

  /**
   * Kicks the specified agent from the specified group.
   * <p/>
   *
   * @param caller the requesting {@link Agent}.
   * @param gname  the group name.
   * @param aid    the {@link AgentIdentifier} specifying the agent to be invited.
   * @throws SecurityException  if the calling {@link Agent} is not authentic or
   *                            not authorized.
   * @throws MessagingException if an error occurs while executing this action.
   */
  public void kickFromGroup(Agent caller, String gname, AgentIdentifier aid)
      throws SecurityException, MessagingException;

  /**
   * Allows a present agent to join the specified group.
   * </p>
   * If the group is invite only, the agent must have been invited by an
   * authorized member of the group (may be the moderator or any member,
   * depending on the group).
   *
   * @param p     the requesting agent's {@link Presence}.
   * @param gname the groups name.
   * @throws SecurityException  if the calling {@link Agent} is not authentic or
   *                            not authorized.
   * @throws MessagingException if an error occurs while executing this action.
   */
  public void joinGroup(Presence p, String gname)
      throws SecurityException, MessagingException;

  /**
   * Allows a present agent to leave the specified group.
   *
   * @param p     the requesting agent's {@link Presence}.
   * @param gname the group name.
   * @throws SecurityException  if the calling {@link Agent} is not authentic or
   *                            not authorized.
   * @throws MessagingException if an error occurs while executing this action.
   */
  public void leaveGroup(Presence p, String gname)
      throws SecurityException, MessagingException;

  /**
   * Factory method for {@link InteractiveMessage} instances to be send to
   * groups.
   *
   * @param p     a previously registered {@link Presence}.
   * @param gname the group name.
   * @param id    the identifier to be used for the created message; if null, the identifier
   *              will be created and set automatically (UUID).
   * @return an {@link EditableInteractiveMessage}.
   * @throws NoSuchGroupException if a group with the given name does not exist.
   * @throws MessagingException   if a group with the given name already exists.
   */
  public EditableInteractiveMessage createGroupMessage(Presence p, String gname, String id)
      throws NoSuchGroupException, MessagingException;

  /**
   * Sends a given {@link Message}.
   * <p/>
   *
   * @param p     a previously registered {@link Presence}.
   * @param gname the group name.
   * @param im    the {@link InteractiveMessage} to be sent.
   * @throws MessagingException    if the given presence is not registered.
   * @throws IllegalStateException if the message was not created using
   *                               {@link #createGroupMessage(Presence,String,String)}.
   * @see #createGroupMessage(Presence,String,String)
   */
  public void send(Presence p, String gname, InteractiveMessage im)
      throws MessagingException, IllegalStateException;

  /**
   * Tests if the given group is a permanent group.
   * @param caller the requesting {@link Agent}.
   * @param gname  the group name.
   * @return true if permanent, false otherwise.
   * @throws SecurityException    if the calling agent is not authentic or authorized.
   * @throws MessagingException   if an error occurs loading the group.
   * @throws NoSuchGroupException if a group with the given name does not exist.
   */
  public boolean isPermanentGroup(Agent caller, String gname)
      throws SecurityException, MessagingException, NoSuchGroupException;

  /**
   * Tests if the given {@link AgentIdentifier} identifies this service.
   * @param aid an {@link AgentIdentifier}.
   * @return true if identifies this service, false otherwise.
   */
  public boolean isGroupMessagingAgent(AgentIdentifier aid);

}//interface GroupMessagingService
