/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.messaging.service;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.Service;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.messaging.model.*;
import net.coalevo.presence.model.Presence;
import java.util.Set;

/**
 * Defines the interface for a messaging service.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface MessagingService extends Service {

  /**
   * Registers with this <tt>MessagingService</tt>.
   * <p/>
   * Note that upon successful registration of a {@link Presence}
   * with a session, a listener will be attached that automatically
   * handles session invalidation.
   * </p>
   *
   * @param p   a {@link Presence}.
   * @param msl an {@link MessagingServiceListener}.
   * @return true if successful, false otherwise.
   */
  public boolean register(Presence p, MessagingServiceListener msl);

  /**
   * Unregisters the given {@link Presence}.
   *
   * @param p a previously registered {@link Presence}.
   * @throws IllegalStateException if the {@link Presence} instance was not obtained from
   *                               this <tt>MessagingService</tt> or is still present.
   */
  public void unregister(Presence p);

  /**
   * Sends the given {@link Message}.
   *
   * @param p a previously registered {@link Presence}.
   * @param m the {@link Message} to be sent.
   * @throws MessagingException if the given presence is not registered, or the
   *                            addressee is not available.
   */
  public void send(Presence p, Message m)
      throws MessagingException;

  /**
   * Broadcasts a given {@link InteractiveMessage} if the calling
   * {@link Agent} is authorized.
   *
   * @param p a previously registered {@link Presence}.
   * @param m the {@link InteractiveMessage} to be broadcasted.
   * @throws SecurityException if the caller is not authentic or authorized to
   *                           broadcast.
   * @throws MessagingException if the given presence is not registered, or the
   *                            broadcasting fails.
   */
  public void broadcast(Presence p, InteractiveMessage m)
      throws SecurityException, MessagingException;

  /**
   * Multicasts a given {@link InteractiveMessage} to the specified
   * receivers if the calling {@link Agent} is authorized.
   *
   * @param p a previously registered {@link Presence}.
   * @param m the {@link InteractiveMessage} to be broadcasted.
   * @param receivers a set of receivers identifying agents.
   * @throws SecurityException if the caller is not authentic or authorized to
   *                           multicast.
   */
   public void multicast(Presence p, InteractiveMessage m, Set<AgentIdentifier> receivers)
      throws SecurityException;

  /**
   * Unicasts a given {@link InteractiveMessage} to the specified receiver,
   * if the calling {@link Agent} is authorized.
   * <p>
   * Note that this may be used by services and admins to override standard
   * sending checks for messages (e.g. the presence status is ignored)..
   * </p>
   *
   * @param p a previously registered {@link Presence}.
   * @param m the {@link InteractiveMessage} to be broadcasted.
   * @throws SecurityException if the caller is not authentic or authorized to
   *                           unicast.
   */
  public void unicast(Presence p, InteractiveMessage m)
      throws SecurityException;

  /**
   * Confirms a {@link InteractiveMessage}.
   *
   * @param p    a previously registered {@link Presence}.
   * @param info an {@link EventInfo} instance.
   * @return the timestamp of the confirmation in millis from UTC.
   * @throws MessagingException if the confirmation fails.
   */
  public long confirm(Presence p, EventInfo info)
      throws MessagingException;

  /**
   * Propagates an event that the calling presence has started
   * a given {@link InteractiveMessage}.
   *
   * @param p    a previously registered {@link Presence}.
   * @param info an {@link EventInfo} instance.
   * @throws MessagingException if the notification fails.
   */
  public void started(Presence p, EventInfo info)
      throws MessagingException;

  /**
   * Propagates an event that the calling presence has stopped
   * a given {@link InteractiveMessage}.
   *
   * @param p    a previously registered {@link Presence}.
   * @param info an {@link EventInfo} instance.
   * @throws MessagingException if the notification fails.
   */
  public void stopped(Presence p, EventInfo info)
      throws MessagingException;

  /**
   * Stores an {@link InteractiveMessage} for the specified receiver.
   * <p>
   * Note that storage may be temporal and limited by the implementation.
   * </p>
   *
   * @param p    a previously registered {@link Presence}.
   * @param im the {@link InteractiveMessage} to be broadcasted.
   * @return true if stored, false if not, which should indicate that the
   *         recipient is actually available, or not subscribed by you.
   * @throws MessagingException if the storage operation fails.
   */
  public boolean store(Presence p, EditableInteractiveMessage im)
      throws MessagingException;

  /**
   * Retrieves offline messages stored for the requester.
   * <p>
   * Note that the retrieval is done by the service and the messages
   * are delivered to the requester through the standard messaging
   * mechanism.
   * </p>
   * @param p a previously registered {@link Presence}.
   * @throws MessagingException if the presence is not registered with this service.
   */
  public void retrieve(Presence p) throws MessagingException;

  /**
   * Factory method for {@link InteractiveMessage} instances with
   * a given type.
   *
   * @param p    a previously registered {@link Presence}.
   * @param type a {@link InteractiveMessageType}.
   * @param id   the identifier to be used for the created message; if null, the identifier
   *             will be created and set automatically (UUID).
   * @return an {@link EditableInteractiveMessage}.
   * @throws MessagingException if creating the message fails.
   */
  public EditableInteractiveMessage create(Presence p, InteractiveMessageType type, String id)
      throws MessagingException;

}//interface MessagingService
